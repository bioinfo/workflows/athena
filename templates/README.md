## athENA template file

This directory contains the athENA metadata template file. 

Relying on a Excel file, it aims at collecting metadata information following [ENA standard](https://www.ebi.ac.uk/training/online/courses/ena-quick-tour/what-is-ena/what-can-i-do-with-resource-name/).

**Important notice**: this athENA metadata template file aims at providing a common framework for most widely used marine sequencing projects at [Ifremer](https://en.ifremer.fr/), such as metabarcoding, RNASeq and WGS. For other use, it may require some adjustments.

## Filling instructions

Please, refer to [athENA wiki](https://gitlab.ifremer.fr/bioinfo/workflows/athena/-/wikis/athENA-spreadsheet-filling-guidelines) to review how to fill in this metadata file.
