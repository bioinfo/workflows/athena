# **athENA: FAIR (meta)data management & automatic submission to EBI-ENA**.


[![Developers](https://img.shields.io/badge/Developers-SeBiMER-yellow?labelColor=000000)](https://ifremer-bioinformatics.github.io/)
[![Nextflow](https://img.shields.io/badge/nextflow-%E2%89%A522.10.0-23aa62.svg?labelColor=000000)](https://www.nextflow.io/)

<img src="assets/logo-athena.png" alt="athena-logo" width="25%" />

## Introduction

athENA is a data brokering protocol designed to collect, validate, and submit sequencing data to the [EBI-ENA international database](https://www.ebi.ac.uk/ena).

The protocol is composed of two main components:

- A metadata file, referred to as the athENA template, which is used to capture sequencing metadata following the [EBI-ENA standard](https://www.ebi.ac.uk/training/online/courses/ena-quick-tour/what-is-ena/what-can-i-do-with-resource-name/);
- A Nextflow pipeline that validates this metadata file and then submits all the information (both sequencing data and metadata) to the ENA server using the [European Nucleotide Archive (ENA)](https://www.ebi.ac.uk/ena) programmatic submission service.

We build upon the work of others; for more details, see the Credits section below.

## FAIR sequencing data management using athENA

At [Ifremer](https://en.ifremer.fr/), athENA protocol is used in production to handle all sequencing projects in a standardized (and FAIR) way.

Here is the example of the LIVACID RNAseq data project:

1. Project metadata is available directly from Ifremer Oceanographic Data Centre (a CoreTrustSeal Certified service) [using a DOI](https://doi.org/10.12770/5b94a252-13df-421e-ac39-1301912f90e2);
2. Project data can be downloaded from [Ifremer Data Server](https://data-dataref.ifremer.fr/bioinfo/ifremer/pfom/livacid/); 
3. Same project has been made available from EBI too, thanks to athENA pipeline: [PRJEB59362](https://www.ebi.ac.uk/ena/browser/view/PRJEB59362)

## Usage: how to?

Using athENA protocol aims at running two successive steps:

1. filling in an athENA template file;
2. executing the athENA pipeline on that file to validate and submit all sequencing data at EBI.

## athENA metadata file overview

[Please follow this link](templates).

## athENA Pipeline overview

[Please follow this link](https://gitlab.ifremer.fr/bioinfo/workflows/athena/-/wikis/athENA%20Pipeline%20overview%20and%20usage)

## Resources

Learn more about about ENA submission services :
- [ENA training modules](http://ena-docs.readthedocs.io/en/latest/index.html).
- [ENA metadata model](https://ena-docs.readthedocs.io/en/latest/_images/metadata_model_study.png).

## Credit

This project has been initiated under the supervision of [Julie Clément](https://www.igh.cnrs.fr/fr/institut/annuaire?acc=true&lastname=Clement&firstname=Julie) and [Bernard de Massy](https://www.igh.cnrs.fr/fr/institut/annuaire?acc=true&lastname=de%20Massy&firstname=Bernard) from [IGH](https://www.igh.cnrs.fr/fr/recherche/departements/dynamique-du-genome/meiose-et-recombinaison#service/).
The pipeline is based on [ena-submit](https://github.com/bigey/ena-submit) pipeline written by [Frédéric Bigey](https://www.researchgate.net/profile/Frederic-Bigey).
Many thanks to all of you.

## Contributions

We welcome contributions to the pipeline. If such case you can do one of the following:
* Use issues to submit your questions
* Fork the project, do your developments and submit a pull request
* Contact us (see email below)

## Support

For further information or help, don't hesitate to get in touch with the athENA developpers:
![sebimer email](assets/sebimer-email.png)


