#!/usr/bin/env nextflow
/*
========================================================================================
                        athENA : Automatic submission To ENA
========================================================================================
 Workflow for automatic submission of sequencing data and metadata to ENA.
 #### Homepage / Documentation
 https://gitlab.ifremer.fr/bioinfo/workflows/athena
----------------------------------------------------------------------------------------
*/

nextflow.enable.dsl=2
import java.time.*
import static java.time.format.DateTimeFormatter.ofPattern

def helpMessage() {
    // Add to this help message with new command line parameters
    log.info SeBiMERHeader()
    log.info"""

    Usage:

    The typical command for running the pipeline is as follows:

        nextflow run main.nf -c cluster.config -profile singularity [params]

    using following [params] :

Input / Ouptut parameters:
--spreadsheet	        [path]	REQUIRED Path to spreadsheet.
--data_alias		[str]	REQUIRED Alias for sequencing dataset.
--md5sum_file		[path]  REQUIRED IF computeMD5 is false. Path to fastq file md5sum list.
--path_prefix		[str]	REQUIRED fastq file path prefix if path is not absolute in Excel metadata for fastq files.
--outdir                [path]  The output directory where the results will be saved.
--projectName           [str]   Name of the project (default: athENA).


Pipeline mode selection:
--mode			[str]	Pipeline mode, 3-character-string : first letter is generateXML (y/n), second letter is uploadFiles (y/n), third letter is ENAsubmission (y/n) (default: 'ynn')
--computeMD5            [bool]  True to compute MD5 checksums for files, false otherwise (default: false).
--action                [str]   Submission action, options: VALIDATE, ADD, or MODIFY (default: 'VALIDATE').
--test                  [bool]  Submit to TEST server if true, to PRODUCTION if false (default: false).
--ignore_sub_status	[bool]  Ignore user-selected submission statusand process all samples as submittable (default: false)
--is_brokering_submission	[bool]	True if submission is done using an ENA broker account (default: true)


XML generation parameters:
--max_study		[int]	Number of study(ies) to publish (default: 1)
--is_run_converted      [bool]  Generate run xml file (default: true)
--is_sample_converted   [bool]  Generate sample xml file (default: true)
--is_experiment_converted       [bool] Generate experiment xml file (default: true)
--is_project_converted  [bool]  Generate project xml file (default: true)


Upload parameters:
--ena_ftp		[url]	ENA Webin URL (default : 'ftp://webin.ebi.ac.uk/')
--ena_creds		[path]	REQUIRED IF mode = *y* or **y. Path to credentials file for ENA connection, expected format is one-line text file 'username password' 
--files2upload		[path]	REQUIRED IF mode = nny / nyn / nyy. Path to text file listing files to upload to ENA server 


Submission parameters:
--xml_dir		[path]	REQUIRED IF mode = nny / nyn / nyy. Path to directory containing XML files (default: '${params.outdir}/${params.results_dirname}/XML')
--url_test		[url]	ENA TEST server URL (default: 'https://wwwdev.ebi.ac.uk/ena/submit/drop-box/submit/')
--url_prod		[url]	ENA PRODUCTION server URL (default: 'https://www.ebi.ac.uk/ena/submit/drop-box/submit/')
--release_date		[date]	Date to release data, expected format DD-MM-YYYY, default is today+2 years)
--center_name		[str]	REQUIRED IF is_brokering_submission is True. Submitter (not broker) center name (default: '')
--get_project_id	[bool]  Submit project XML to ENA to get project accession ID (default: true).
--get_sample_id		[bool]  Submit sample XML to ENA to get sample accession ID (default: true).
--get_experiment_id	[bool]  Submit experiment XML to ENA to get experiment accession ID (default: true).
--get_run_id		[bool]  Submit run XML to ENA to get run accession ID (default: true).


Other options:
-w/--workdir            [path]	The temporary directory where intermediate data will be saved.
-name                   [str]	Name for the pipeline run. If not specified, Nextflow will automatically generate a random mnemonic.

        """.stripIndent()
}

// Show help message
if (params.help) {
    helpMessage()
    exit 0
}

// SET UP CONFIGURATION VARIABLES
// Has the run name been specified by the user?
// this has the bonus effect of catching both -name and --name
custom_runName = params.name
if (!(workflow.runName ==~ /[a-z]+_[a-z]+/)) {
    custom_runName = workflow.runName
}

//Copy config files to output directory for each run
paramsfile = file("$baseDir/conf/base.config", checkIfExists: true)
paramsfile.copyTo("$params.outdir/00_pipeline_conf/base.config")

// PIPELINE INFO
// Header log info
log.info SeBiMERHeader()
def summary = [:]
if (workflow.revision) summary['Pipeline Release'] = workflow.revision
summary['Run Name'] = custom_runName ?: workflow.runName
summary['Project Name'] = params.projectName
summary['Output dir'] = params.outdir
summary['Launch dir'] = workflow.launchDir
summary['Working dir'] = workflow.workDir
summary['User'] = workflow.userName
summary['Input file'] = params.spreadsheet
summary['Path prefix'] = params.path_prefix
summary['Run mode'] = params.mode
summary['Submission action'] = params.action
summary['Release date'] = params.release_date
summary['Credential file'] = params.ena_creds
log.info summary.collect { k,v -> "${k.padRight(18)}: $v" }.join("\n")
log.info "-\033[91m--------------------------------------------------\033[0m-"

// Check hostnames against configured profiles
checkHostname()

/*
 * CHECK AND SET UP WORKFLOW VARIABLES
 */
// Input data
Channel
  .fromPath( params.spreadsheet , checkIfExists: true )
  .set { XL_file }

Channel
  .from( params.data_alias )
  .combine(XL_file)
  .set { XL_ch }

Channel
  .fromPath( params.ena_creds, checkIfExists: true )
  .set { creds_ch }

// Parameter validation
if ( params.mode.toString() != "yyy" &&  params.mode.toString() != "nnn" &&  params.mode.toString() != "ynn" &&  params.mode.toString() != "yyn" &&  params.mode.toString() != "nny" &&  params.mode.toString() != "nyy" &&  params.mode.toString() != "nyn" &&  params.mode.toString() != "yny" ) { println("Parameter 'mode' should be either 'yyy'; 'yyn'; 'nyy'; 'yny'; 'ynn'; 'nyn'; 'nny'; 'nnn' but '${params.mode}' was given. See 'nextflow run --help' to learn more about 'mode' parameter"); exit 1 }

Boolean validate = false
Boolean upload = false
Boolean submit = false
Boolean md5 = false

// Note : I wanted to encode using 0 or 1 instead of y or n ; but Nextflow does not handle properly when parameter begins with 0 !
if ( params.mode.toString()[0] == "y" ) { validate = true }
if ( params.mode.toString()[1] == "y" ) { upload = true }
if ( params.mode.toString()[2] == "y" ) { submit = true }

if ( params.computeMD5.toString().toLowerCase() == "true" ) { md5 = true } else { md5 = false }

if ( ! params.computeMD5 && params.md5sum_file != "" && params.md5sum_file != "/") {
    Channel
      .fromPath( params.md5sum_file , checkIfExists: true )
      .set { md5_ch }
}
else {
    if ( ! params.computeMD5 ) {
        println("computeMD5 option is false, but no md5sum_file was given. Please set up md5sum_file parameter correctly.");  exit 1
    }
    else {
        Channel.empty().set { md5_ch }
    }
}

//if ( params.computeMD5.toString() == "True" || params.computeMD5.toString() == "true" ) { md5 = true }

// Check if --ena_creds parameter is set when needed
if ( upload && !params.ena_creds ) { println("To upload files you need to set parameter 'ena_creds' (a file containing you ENA account login and password, 1 line, space-separated"); exit 1 }
if ( submit && !params.ena_creds ) { println("To submit records you need to set parameter 'ena_creds' (a file containing you ENA account login and password, 1 line, space-separated"); exit 1 }

// Check if validate/upload/submit options are compatible with other parameters
if ( !validate && upload && !params.files2upload) { println("Validate option is false, upload option is true, you selected ny* mode, but no files2upload was provided. Please set up files2upload parameter.");  exit 1 }
if ( !validate && submit && !params.xml_dir) { println("Validate option is false, submit option is true, you selected n*y mode, but no xml_dir was provided. Please set up xml_dir parameter.");  exit 1 }
if ( !validate && !upload && !params.xml_dir) { println("Validate option is false, upload option is false, submit option is true, you selected n*y mode, but no xml_dir was provided. Please set up xml_dir parameter.");  exit 1 }

/*
 * IMPORTING MODULES
 */
include { generateXML }		from './modules/generate-xml.nf'
include { generatefileList }    from './modules/generate-xml.nf'
include { uploadFiles }		from './modules/upload-files.nf'
include { ENAsubmission }	from './modules/submit-to-ena.nf'
include { checkSubmission }	from './modules/submit-to-ena.nf'
include { computeMD5sum }     	from './modules/compute-md5sum.nf'
include { collectMD5sum }       from './modules/compute-md5sum.nf'
include { checkEmptyFastq }	from './modules/integrity-check.nf'

/*
 * INITIALIZE CHANNELS
 */
up_ch = Channel.from(null)
f2u_file = Channel.from(null)
def url = params.url_test

/*
 * RUN MAIN WORKFLOW
 */
workflow {

    /****************************************/
    /* STEP 0 : OPTIONAL MD5SUM COMPUTATION */
    /****************************************/
    // From Excel athENA file, get fastq file list
    generatefileList(XL_ch)

    // Split fastq file list to parallelize md5sum computation
    generatefileList.out.upstream_files_ch
        .splitText()
        .map { it.trim() }
        .map { file(it) }
        .set { file_list_ch }

    // Check if file is empty
    checkEmptyFastq(file_list_ch)

    /* IF computeMD5 IS TRUE, Generate list of fastq files then compute md5sum for each file */
    if ( md5 ) {

        // Compute md5sum for each fastq file
        computeMD5sum(file_list_ch)

        // Collect all md5sum in one single file
        collectMD5sum(computeMD5sum.out.MD5_ch.collect())

        // Rename channel
        collectMD5sum.out.allmd5_ch
            .set { step0_ch }
   }
    /* ELSE, do not compute md5 and rename md5 channel (user-provided) */
    else {
        md5_ch
            .set { step0_ch }
    }

    // Create channel for XML generation (STEP 1) 
    // The resulting channel is composed of 3 elements : [alias, athena_file.xlsx, file_md5sum.txt]
    XL_ch        
        .combine(step0_ch)
        .set { step1_ch }

    /********************************************/
    /* STEP 1 : GENERATE AND VALIDATE XML FILES */
    /********************************************/
    /* IF VALIDATE OPTION IS TRUE, Generate XML files from spreadsheet file */
    if ( validate ) {

        // GENERATE XML FROM EXCEL SPREADSHEET
        generateXML(step1_ch)

        // Create channel for upload containing file list to upload
        // The resulting channel is composed of 3 elements : [alias, files_to_upload.txt, ena_credential_file.txt]
        generateXML.out.XML_ch
          .map { it -> [ it[0], it[5] ] }
          .combine(creds_ch)
          .set { up_ch }

        // Create channel for data submission to ENA
        // The resulting channel is composed of 6 elements : [alias, project.xml, sample.xml, experiment.xml, run.xml, files_to_upload.txt, ena_credential_file.txt]
        generateXML.out.XML_ch
          .combine(creds_ch)
          .set { submit_ch }
    }

    /* IF VALIDATE OPTION IS FALSE, When XML files have previously been created */
    else {
        // Build channel for upload step, if needed
        // The resulting channel is composed of 3 elements : [alias, files_to_upload.txt (user provided), ena_credential_file.txt]
        Channel
          .fromPath( params.files2upload )
          .ifEmpty { error "Cannot find input files2upload file: '${params.files2upload}'. Set 'files2upload' parameter with a valid path." }
          .set { f2u_file }

        Channel
          .from( params.data_alias )
          .combine(f2u_file)
          .set { up_tmp_ch }

        up_tmp_ch
          .combine(creds_ch)
          .set { up_ch }       
 
        // Build channel for submit step, if needed
        // The resulting channel is composed of 7 elements : [alias, project.xml (user provided), sample.xml (user provided), experiment.xml (user provided), run.xml (user provided), files_to_upload.txt (user provided), ena_credential_file.txt]
        Channel
          .fromPath( [ "${params.project.xml}", "${params.sample.xml}", "${params.experiment.xml}", "${params.run.xml}" ], checkIfExists: true)
          .set { XML_files }

        Channel
          .from( params.data_alias )
          .concat(XML_files)
          .concat(f2u_file)
          .concat(creds_ch)
          .set { submit_ch }
    }


    /**************************************************/
    /* STEP 2 : UPLOAD FASTQ FILES TO ENA FTP SERVER  */
    /**************************************************/
    /* IF UPLOAD OPTION IS TRUE, UPLOAD FILES TO ENA FTP SERVER */
    if ( upload && params.ena_ftp && params.ena_creds ) {
        // Split Channel to create one job per file to upload
        up_ch
            .splitText()
            .set { up_split_ch }

        // Upload files
        uploadFiles(up_split_ch)
        upok_ch = uploadFiles.out.ML_ch
    }
    /* IF UPLOAD OPTION IS FALSE, CREATE UPOK CHANNEL */
    else {
        upok_ch = Channel.from( params.data_alias )
    }

    /************************************/
    /* STEP 3 : SUBMIT XML FILES TO ENA */
    /************************************/
    /* IF SUBMIT OPTION IS TRUE, SUBMIT RECORDS TO ENA */
    if ( submit && params.ena_ftp && params.ena_creds ) { 
	upok_ch
           .join(submit_ch.collect(), by:0)
           .set { submitok_ch }
        if ( ! params.test ) {
	    url = params.url_prod
        }

        // SUBMIT RECORDS TO ENA
        ENAsubmission(submitok_ch, url, params.release_date, params.center_name)
        // CHECK SUBMISSION REPORT
        checkSubmission(ENAsubmission.out.SUB_ch)
    } 
}



/*
 * Completion notification
 */

workflow.onComplete {
    c_green = params.monochrome_logs ? '' : "\033[0;32m";
    c_purple = params.monochrome_logs ? '' : "\033[0;35m";
    c_red = params.monochrome_logs ? '' : "\033[0;31m";
    c_reset = params.monochrome_logs ? '' : "\033[0m";
    
    def msg = """\
        Pipeline execution summary
        ---------------------------
        Completed at: ${workflow.complete}
        Duration    : ${workflow.duration}
        Success     : ${workflow.success}
        workDir     : ${workflow.workDir}
        exit status : ${workflow.exitStatus}
        """
    .stripIndent()
    //sendMail(to: 'pauline.auffret@ifremer.fr', subject: 'athENA pipeline execution notification', body: msg)
    if (workflow.success) {
        log.info "-${c_purple}[Submission workflow]${c_green} athENA workflow completed successfully${c_reset}-"
    } else {
        checkHostname()
        log.info "-${c_purple}[Submission workflow]${c_red} athENA workflow completed with errors${c_reset}-"
    }
}

/*
 * Other functions
 */

def SeBiMERHeader() {
    // Log colors ANSI codes
    c_red = params.monochrome_logs ? '' : "\033[0;91m";
    c_blue = params.monochrome_logs ? '' : "\033[1;94m";
    c_reset = params.monochrome_logs ? '' : "\033[0m";
    c_yellow = params.monochrome_logs ? '' : "\033[1;93m";
    c_Ipurple = params.monochrome_logs ? '' : "\033[0;95m" ;

    return """    -${c_red}--------------------------------------------------${c_reset}-

    ${c_blue}  ╔═══╗────╔══╗───╔═╗╔═╗╔═══╗╔═══╗    ${c_blue}
    ${c_blue}  ║╔═╗║────║╔╗║───║║╚╝║║║╔══╝║╔═╗║    ${c_blue}
    ${c_blue}  ║╚══╗╔══╗║╚╝╚╗╔╗║╔╗╔╗║║╚══╗║╚═╝║    ${c_blue}
    ${c_blue}  ╚══╗║║║═╣║╔═╗║╠╣║║║║║║║╔══╝║╔╗╔╝    ${c_blue}
    ${c_blue}  ║╚═╝║║║═╣║╚═╝║║║║║║║║║║╚══╗║║║╚╗    ${c_blue}
    ${c_blue}  ╚═══╝╚══╝╚═══╝╚╝╚╝╚╝╚╝╚═══╝╚╝╚═╝    ${c_blue}
    ${c_yellow}  athENA workflow (version ${workflow.manifest.version})${c_reset}
                                            ${c_reset}
    ${c_Ipurple}  Homepage: ${workflow.manifest.homePage}${c_reset}
    -${c_red}--------------------------------------------------${c_reset}-
    """.stripIndent()
}

def checkHostname() {
    def c_reset = params.monochrome_logs ? '' : "\033[0m"
    def c_white = params.monochrome_logs ? '' : "\033[0;37m"
    def c_red = params.monochrome_logs ? '' : "\033[1;91m"
    def c_yellow_bold = params.monochrome_logs ? '' : "\033[1;93m"
    if (params.hostnames) {
        def hostname = "hostname".execute().text.trim()
        params.hostnames.each { prof, hnames ->
            hnames.each { hname ->
                if (hostname.contains(hname) && !workflow.profile.contains(prof)) {
                    log.error "====================================================\n" +
                            "  ${c_red}WARNING!${c_reset} You are running with `-profile $workflow.profile`\n" +
                            "  but your machine hostname is ${c_white}'$hostname'${c_reset}\n" +
                            "  ${c_yellow_bold}It's highly recommended that you use `-profile $prof${c_reset}`\n" +
                            "============================================================"
                }
            }
        }
    }
}


