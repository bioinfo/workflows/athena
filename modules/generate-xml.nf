process generateXML {

    tag "${XL_id}"
    label 'generateXML'
    label 'athena1'
    label 'internet_access'


    publishDir "${params.outdir}/${params.results_dirname}/XML", \
		mode: 'copy', pattern: '*.xml'
    publishDir "${params.outdir}/${params.results_dirname}/upload", \
                mode: 'copy', pattern: '*.txt'
    publishDir "${params.outdir}/${params.results_dirname}/logs", \
		mode: 'copy', pattern: '*generateXML.log'

    input:
        tuple val(XL_id), path(XL_ch), path(md5_ch)

    output:
        tuple	val("${XL_id}"), \
		path("*project.xml"), \
		path("*sample.xml"), \
		path("*experiment.xml"), \
		path("*run.xml"), \
		path("*files_list.txt"), \
		emit: XML_ch
        path("*generateXML.log")

    script:
    """
    python3 ${baseDir}/bin/generate-xml.py	--input_file $XL_ch \
                        	--md5_file $md5_ch \
                        	--path_prefix ${params.path_prefix} \
				--data_file_name ${XL_id}_files_list.txt \
				--max_project ${params.max_study} \
				--ignore_sub_status ${params.ignore_sub_status} \
                        	--is_brokering_submission ${params.is_brokering_submission} \
                        	--is_run_converted ${params.is_run_converted} \
                        	--is_sample_converted ${params.is_sample_converted} \
                        	--is_experiment_converted ${params.is_experiment_converted} \
                        	--is_project_converted ${params.is_project_converted} \
                        	--only_extract_filenames "False" \
				--output_dir . >& ${XL_id}_generateXML.log 2>&1

    # Rename output files
    if [ ${params.is_project_converted} == "True" ]; then mv project.xml ${XL_id}_project.xml; else touch ${XL_id}_project.xml; fi

    if [ ${params.is_sample_converted} == "True" ]; then mv sample.xml ${XL_id}_sample.xml; else touch ${XL_id}_sample.xml; fi

    if [ ${params.is_experiment_converted} == "True" ]; then mv experiment.xml ${XL_id}_experiment.xml; else touch ${XL_id}_experiment.xml; fi

    if [ ${params.is_run_converted} == "True" ]; then mv run.xml ${XL_id}_run.xml; else touch ${XL_id}_run.xml; fi

    if [ ! -f ${XL_id}_files_list.txt ]; then
        touch ${XL_id}_files_list.txt;
    fi

    """
}

process generatefileList {

    tag "${XL_id}"
    label 'generatefileList'
    label 'athena1'

    publishDir "${params.outdir}/${params.results_dirname}/md5sum", \
                mode: 'copy', pattern: '*.txt'
    publishDir "${params.outdir}/${params.results_dirname}/logs", \
                mode: 'copy', pattern: '*generatefileList.log'

    input:
        tuple val(XL_id), path(XL_ch)

    output:
        path("*files_to_md5.txt"), \
                emit: upstream_files_ch
        path("*generatefileList.log")

    script:
    """
    python3 ${baseDir}/bin/generate-xml.py     --input_file $XL_ch \
                        	--md5_file "" \
                        	--path_prefix ${params.path_prefix} \
                        	--data_file_name ${XL_id}_files_list.txt \
                        	--max_project ${params.max_study} \
                        	--ignore_sub_status ${params.ignore_sub_status} \
                        	--is_brokering_submission ${params.is_brokering_submission} \
                        	--is_run_converted "False" \
                        	--is_sample_converted "False" \
                        	--is_experiment_converted "False" \
                        	--is_project_converted "False" \
                        	--only_extract_filenames "True" \
                        	--upstream_data_file_name ${XL_id}_files_to_md5.txt \
                        	--output_dir . >& ${XL_id}_generatefileList.log 2>&1

    """
}

