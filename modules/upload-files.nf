process uploadFiles {

    tag "${XL_id}"
    label 'uploadFiles'
    label 'internet_access'
    label 'athena1'

    publishDir "${params.outdir}/${params.results_dirname}/logs", \
		mode: 'copy', pattern: '*uploadFiles.log'

    input:
        tuple val(XL_id), path(files_list_ch), path(ena_creds)

    output:
        tuple	val("${XL_id}"), \
		emit: ML_ch
        path("*uploadFiles.log")

    script:
    """
    upload-files.sh	$files_list_ch \
			${params.ena_ftp} \
			$ena_creds \
			>& ${XL_id}_uploadFiles.log 2>&1
    """
}


