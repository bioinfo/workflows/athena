process computeMD5sum {

    label 'md5'
    label 'athena1'

    publishDir "${params.outdir}/${params.results_dirname}/md5sum", \
                mode: 'copy', pattern: '*.md5'
    publishDir "${params.outdir}/${params.results_dirname}/logs/computeMD5sum", \
		mode: 'copy', pattern: '*computeMD5sum.log'

    input:
        path(in_file)

    output:
        path("*.md5"), emit: MD5_ch
        path("*computeMD5sum.log")

    script:
    """
    if [ -f $in_file ]; then
        md5sum $in_file > ${in_file.baseName}.md5 2> ${in_file.baseName}_computeMD5sum.log
        echo "md5sum for file $in_file computed successfully" > ${in_file.baseName}_computeMD5sum.log
    else
        echo "File $in_file does not exist!" > ${in_file.baseName}_computeMD5sum.log
        touch ${in_file.baseName}.md5
    fi
    """
}

process collectMD5sum {

    label 'collect'
    label 'athena1'

    publishDir "${params.outdir}/${params.results_dirname}/md5sum", \
                mode: 'copy', pattern: 'allfiles.md5'
    publishDir "${params.outdir}/${params.results_dirname}/logs/computeMD5sum", \
                mode: 'copy', pattern: '*collectMD5sum.log'

    input:
        path(in_file)

    output:
        path("*.md5"), emit: allmd5_ch
        path("*collectMD5sum.log")

    script:
    """
    cat *.md5 > allfiles.md5 2> collectMD5sum.log
    """
}


