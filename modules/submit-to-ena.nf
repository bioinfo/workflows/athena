process ENAsubmission {

    tag "${XL_id}"
    label 'ENAsubmission'
    label 'internet_access'
    label 'athena1'
    
    publishDir "${params.outdir}/${params.results_dirname}/receipts", \
		mode: 'copy', pattern: '*.xml'
    publishDir "${params.outdir}/${params.results_dirname}/logs", \
		mode: 'copy', pattern: '*ENAsubmission.log'

    input:
	tuple   val(XL_id), \
                path(project), \
                path(sample), \
                path(experiment), \
                path(run), \
                path(files_list), \
                path(ena_creds)
	val(url)
        val(release_date)
 	val(centre_name)

    output:
        tuple	val("${XL_id}"), \
		path("*submission-receipt.xml"), \
		emit: SUB_ch
        path("*ENAsubmission.log")

    script:
    """
    submit-to-ena.sh	$project \
			$sample \
			$experiment \
			$run \
			${params.action} \
			$url \
			$release_date \
			$centre_name \
			$ena_creds \
			${XL_id}_${params.action}_submission-receipt.xml \
                        ${params.get_project_id} \
                        ${params.get_sample_id} \
                        ${params.get_experiment_id} \
                        ${params.get_run_id} \
			>& ${XL_id}_ENAsubmission.log 2>&1
    """
}

process checkSubmission {

    tag "${XL_id}"
    label 'checkSubmission'
    label 'athena1'
    
    publishDir "${params.outdir}/${params.results_dirname}/logs", \
                mode: 'copy', pattern: '*checkSubmission.log'
    publishDir "${params.outdir}/${params.results_dirname}/receipts", \
                mode: 'copy', pattern: '*txt'

    input:
	tuple 	val(XL_id), \
		path(submission_receipt)

    output:
        tuple	val("${XL_id}"), \
		path("*.txt"), optional: true, \
		emit: check_ch
	path("*checkSubmission.log")

    script:
    """
    if grep 'success=\"true\"' $submission_receipt; then
        echo "Submission was successful." ;
            if [[ "${params.action}" == "ADD" ]] ; then
    	        parse-receipt.py -t -o "${submission_receipt.baseName}.txt" $submission_receipt >& ${XL_id}_checkSubmission.log 2>&1 ;
            else
                echo "Submission was successful : check ${params.outdir}/${params.results_dirname}/receipts/${submission_receipt} file." >& ${XL_id}_checkSubmission.log 2>&1 ;
            fi
    else 
	echo "Submission was not successful : check ${params.outdir}/${params.results_dirname}/receipts/${submission_receipt} file." >& ${XL_id}_checkSubmission.log 2>&1 ;
	exit 1;
    fi

    """
}

