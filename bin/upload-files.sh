#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                    Upload sequence files to ENA FTP server                ##
##                                                                           ##
###############################################################################
# var settings
args=("$@")
FILES_TO_UPLOAD=${args[0]}
ENA_FTP=${args[1]}
ENA_CRED=${args[2]}

# Read user and password from credentials file
read USR PWD < $ENA_CRED

# Upload files using cURL
curl	--user $USR:$PWD \
	--upload-file "{${FILES_TO_UPLOAD}}"\
	--url $ENA_FTP 


#######################################################
########              HELP SECTION             ########
#######################################################
#DESCRIPTION
#       curl  is  a  tool  to  transfer data from or to a server, using one of the supported protocols (DICT, FILE, FTP, FTPS, GOPHER, HTTP, HTTPS, IMAP, IMAPS, LDAP, LDAPS, POP3, POP3S,
#       RTMP, RTSP, SCP, SFTP, SMTP, SMTPS, TELNET and TFTP).  The command is designed to work without user interaction.
#
#       curl offers a busload of useful tricks like proxy support, user authentication, FTP upload, HTTP post, SSL connections, cookies, file transfer resume, Metalink, and more. As  you
#       will see below, the number of features will make your head spin!
#
#       curl is powered by libcurl for all transfer-related features. See libcurl(3) for details.
#
#[...]
#       -T, --upload-file <file>
#              This  transfers  the  specified local file to the remote URL. If there is no file part in the specified URL, Curl will append the local file name. NOTE that you must use a
#              trailing / on the last directory to really prove to Curl that there is no file name or curl will think that your last directory name is the remote file name to  use.  That
#              will most likely cause the upload operation to fail. If this is used on an HTTP(S) server, the PUT command will be used.
#
#              Use  the file name "-" (a single dash) to use stdin instead of a given file.  Alternately, the file name "." (a single period) may be specified instead of "-" to use stdin
#              in non-blocking mode to allow reading server output while stdin is being uploaded.
#
#              You can specify one -T for each URL on the command line. Each -T + URL pair specifies what to upload and to where. curl also supports "globbing" of the -T argument,  mean-
#              ing that you can upload multiple files to a single URL by using the same URL globbing style supported in the URL, like this:
#
#              curl -T "{file1,file2}" http://www.uploadtothissite.com
#
#              or even
#
#              curl -T "img[1-1000].png" ftp://ftp.picturemania.com/upload/
#
#       -u, --user <user:password>
#              Specify the user name and password to use for server authentication. Overrides -n, --netrc and --netrc-optional.
#
#              If you simply specify the user name, curl will prompt for a password.
#
#              If you use an SSPI-enabled curl binary and perform NTLM authentication, you can force curl to select the user name and password from your environment by specifying a  sin-
#              gle colon with this option: "-u :".
#
#              If this option is used several times, the last one will be used.
#
#       -U, --proxy-user <user:password>
#              Specify the user name and password to use for proxy authentication.
#
#             If  you  use  an SSPI-enabled curl binary and do NTLM authentication, you can force curl to pick up the user name and password from your environment by simply specifying a
#              single colon with this option: "-U :".
#
#              If this option is used several times, the last one will be used.
#
#       --url <URL>
#              Specify a URL to fetch. This option is mostly handy when you want to specify URL(s) in a config file.
#
#              This option may be used any number of times. To control where this URL is written, use the -o, --output or the -O, --remote-name options.
#
#[...]
#######################################################
