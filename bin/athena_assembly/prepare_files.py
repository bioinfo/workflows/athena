#!/usr/bin/env python
"""
This script aims to prepare the file needed for the Genome Assemblies submission the webin-cli tool

To do so, this script uses the athena excel file from which it will generate the necessary documents (manifest file, chromosomes file) for each assembly.
automate the creation of manifest file et chromosomes list file detailled in the athena excel document. These files are then used for the assembly submission procedure using the webin-cli tool

usage:
python3 genomENA.py -a 2023_assembly-chromosomes_seabiomic.xlsx -o outdir_path

Author: Alizee Bardon

contact : alizee.bardon@gmail.com
"""

# To handle Excel and csv file formats
from openpyxl import load_workbook
import csv

# To add arguments to command line 
import argparse

# To execute bash commands (e.g. symlink)
import os

 ##################################################################################################

 ##################################################################################################

# Function to read input athena Excel file
def load_assemblies_excel(path_athena_assembly) :
    print("\nLoading 2023_assembly-metadata-template_ATHENA.xlsx")
    workbook = load_workbook(filename=path_athena_assembly)
    return workbook

# Function to pair column names with their indices and store in a dictionary
def defines_column_names(header_row) :
    ColNames_index = {}
    for  metadata_field in header_row[1:] :
        if metadata_field.value == None :
            break
        ColNames_index[metadata_field.value] = metadata_field.column
    return ColNames_index

# Function to create fasta file symlink in the working directory
# DO NOT WORK : FORCED TO CREATE A COPY IN A DIRECTORY WHERE USER HAS WRITING PERMISSIONS
def setting_fasta_symlink(fasta_cell, output_dir) :
    # Making a symlink of the genome in the output directory
    fasta_path = fasta_cell.value
    if not os.path.exists(fasta_path) :
        raise ValueError("The fasta path provided,", fasta_path ,", doesn't match an existing file")

    fasta_symlink = output_dir + fasta_path.split("/")[-1]
    if os.path.islink(fasta_symlink) :
        print("The symlink ", fasta_symlink, " already exist")
    else :
        #os.symlink(fasta_path, fasta_symlink)
        os.system("cp "+fasta_path+" "+fasta_symlink)
        print("fasta_path symlink created in the ouput directory ( "+ fasta_symlink + " )")

    # Replacing the genomes original path by the symplink in the assembly info
    fasta_cell.value = fasta_symlink


def write_manifest_file_for_each_assembly(sheet_MANIFEST, output_dir) :

    ColNames_MANIFEST_file = defines_column_names(sheet_MANIFEST[4])

    assemblies_name_list = []
    # writing the manifest for each assembly
    while sheet_MANIFEST.cell(row=5+len(assemblies_name_list), column=ColNames_MANIFEST_file['ASSEMBLYNAME']).value  != None :
        assemblies_name_list.append(
            sheet_MANIFEST.cell(row=5+len(assemblies_name_list), column=ColNames_MANIFEST_file['ASSEMBLYNAME']).value
        )

    for assembly_i in range(len(assemblies_name_list)) :
        print("\nWrinting the manifest file for the assembly : ", sheet_MANIFEST.cell(row=5+assembly_i, column=ColNames_MANIFEST_file['ASSEMBLYNAME']).value)
        ASSEMBLY_MANIFEST = output_dir + sheet_MANIFEST.cell(row=5+assembly_i, column=ColNames_MANIFEST_file['ASSEMBLYNAME']).value + "_manifest.tsv"

        setting_fasta_symlink(sheet_MANIFEST.cell(row=5+assembly_i, column=ColNames_MANIFEST_file['FASTA']) , output_dir)

        with open(ASSEMBLY_MANIFEST, 'wt') as manifest:
            tsv_writer = csv.writer(manifest, delimiter='\t')
            for col in ColNames_MANIFEST_file :
                cell = sheet_MANIFEST.cell(row=5+assembly_i, column=ColNames_MANIFEST_file[col]).value
                if col == "FASTA" :
                     cell = os.path.basename(cell)                
                if cell != None :
                    tsv_writer.writerow([ col, cell])
                    

    return assemblies_name_list


def write_chromosomes_list_for_each_assembly(sheet_Chromosome, assemblies_name_list, output_dir) :
    # Setting a dictionary of row names
    ColNames_Chromosome = defines_column_names(sheet_Chromosome[4])
    Nb_chromosomes = conting_chromosomes(sheet_Chromosome, ColNames_Chromosome['CHROMOSOME_NAME'])
    
    for assembly in assemblies_name_list :
        # finding all the chromosomes corresponding to an assembly
        assembly_chromosomes_index = chromosomes_for_an_assembly(assembly, sheet_Chromosome, Nb_chromosomes, ColNames_Chromosome['ASSEMBLYNAME'])
        # writing the chromosomes list file
        if assembly_chromosomes_index != [] :
            chromosomes_list_file = output_dir + assembly + "_chromosome_list_file.tsv"
            with open(chromosomes_list_file, 'wt') as manifest:
                tsv_writer = csv.writer(manifest, delimiter='\t')
                for chr_i in assembly_chromosomes_index :
                    Chromosome_list_file = Chromosome_List_File(sheet_Chromosome, chr_i, ColNames_Chromosome)
                    tsv_writer.writerow(Chromosome_list_file)


def chromosomes_for_an_assembly(assembly, sheet_Chromosome, Nb_chromosomes, ASSEMBLYNAME_column) :
    assembly_chromosomes_index = []
    for chr_i in range(Nb_chromosomes) :
        assembly_chr_i = sheet_Chromosome.cell( row=5+chr_i, column=ASSEMBLYNAME_column )
        if assembly_chr_i.value == assembly :
            assembly_chromosomes_index.append(assembly_chr_i.row)
    return(assembly_chromosomes_index)


def conting_chromosomes(sheet_Chromosome, CHROMOSOME_NAME_column) :
    Nb_chromosomes = 0
    while sheet_Chromosome.cell(
        row=5+Nb_chromosomes,
        column=CHROMOSOME_NAME_column
    ).value  != None :
        Nb_chromosomes +=1
    return Nb_chromosomes

def Chromosome_List_File(sheet_Chromosome, chr_i, ColNames_Chromosome):
    #Chromosome_List_File = [
    #    sheet_Chromosome.cell( row=5+chr_i, column=ColNames_Chromosome['OBJECT_NAME']).value,
    #    sheet_Chromosome.cell( row=5+chr_i, column=ColNames_Chromosome['CHROMOSOME_NAME']).value,
    #    sheet_Chromosome.cell( row=5+chr_i, column=ColNames_Chromosome['CHROMOSOME_TYPE']).value,
    #    sheet_Chromosome.cell( row=5+chr_i, column=ColNames_Chromosome['OBJECT_NAME']).value
    #    ]
    #if sheet_Chromosome.cell( row=5+chr_i, column=ColNames_Chromosome['TOPOLOGY']).value != None :
    #    Chromosome_List_File.append(sheet_Chromosome.cell( row=5+chr_i, column=ColNames_Chromosome['TOPOLOGY']).value)
    #if sheet_Chromosome.cell( row=5+chr_i, column=ColNames_Chromosome['CHROMOSOME_LOCATION'] ).value != None :
    #    Chromosome_List_File.append(sheet_Chromosome.cell( row=5+chr_i, column=ColNames_Chromosome['CHROMOSOME_LOCATION'] ).value)
    Chromosome_List_File = [
        sheet_Chromosome.cell( row=chr_i, column=ColNames_Chromosome['OBJECT_NAME']).value,
        sheet_Chromosome.cell( row=chr_i, column=ColNames_Chromosome['CHROMOSOME_NAME']).value,
        sheet_Chromosome.cell( row=chr_i, column=ColNames_Chromosome['CHROMOSOME_TYPE']).value
        #sheet_Chromosome.cell( row=chr_i, column=ColNames_Chromosome['OBJECT_NAME']).value
        ]
    if sheet_Chromosome.cell( row=chr_i, column=ColNames_Chromosome['TOPOLOGY']).value != None :
        Chromosome_List_File.append(sheet_Chromosome.cell( row=chr_i, column=ColNames_Chromosome['TOPOLOGY']).value)
    if sheet_Chromosome.cell( row=chr_i, column=ColNames_Chromosome['CHROMOSOME_LOCATION'] ).value != None :
        Chromosome_List_File.append(sheet_Chromosome.cell( row=chr_i, column=ColNames_Chromosome['CHROMOSOME_LOCATION'] ).value)
    return Chromosome_List_File

 ##################################################################################################

 ##################################################################################################

if __name__ == '__main__':
    # Create the parser
    parser = argparse.ArgumentParser()

    # Add arguments
    parser.add_argument('-a', '--path_athena_assembly', type=str, required=True,
                        help="Path the assembly athena excel e.g : 'path/to/2023_assembly-chromosomes_seabiomic.xlsx' ")
    parser.add_argument('-o', '--output_dir', type=str, required=True,
                        help="Path to the output directory, where the mafifest files will be written")

    # Parse the argument
    args = parser.parse_args()

    # output directory
    if args.output_dir[-1] != '/' :
        args.output_dir = args.output_dir + '/'

    # Importing the assembly athena excel
    workbook = load_assemblies_excel(args.path_athena_assembly)

    # Selecting the "MANIFEST file" sheet
    sheet_MANIFEST = workbook["MANIFEST file"]

    # Writing the manifest file for each assembly in the "MANIFEST file" sheet
    # this function return a list with all the assemblies
    assemblies_name_list = write_manifest_file_for_each_assembly(sheet_MANIFEST, args.output_dir)


    # Selecting the "Chromosome List File" sheet
    sheet_Chromosome = workbook["Chromosome List File"]

    # writing the "chromosome list file" for each assembly (if needed)
    #write_chromosomes_list_for_each_assembly(sheet_Chromosome, assemblies_name_list, args.output_dir)
