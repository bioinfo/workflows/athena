## Configuration file for generate_xml.py script

ena_taxonomy_api = "https://www.ebi.ac.uk/ena/taxonomy/rest/scientific-name"
zero_threshold = 10
header_xml = "<?xml version=\"1.0\" encoding=\"US-ASCII\"?>"
invalid_first_cell_content = ["0", "", "sam_", "None", "sam_0", "exp_", "exp_sam_", "run_", "run__3"]
invalid_str_cell_content = ["0", ""]
key_row_name = ["alias","ALIAS"]
collaborators_sheet_name = "COLLABORATORS"
study_sheet_name = "STUDY"
key_collab_name = "Last name"
correct_missing_value = "not provided"
wrong_missing_values = ["NA","UNKNOWN","NOT AVAILABLE", "/", "N.A.", "N.A","N/A", "#N/A", "None"]

native_template_sheet_names=["project","sample","experiment","run"]
egide_indiv_template_sheet_names=['STUDY', 'COLLABORATORS', 'SAMPLES_general', 'SAMPLES_indiv', 'SAMPLES_chemphys', 'SEQUENCING']
egide_meta_template_sheet_names=['STUDY', 'COLLABORATORS', 'SAMPLES_general', 'SAMPLES_meta', 'SAMPLES_chemphys', 'SEQUENCING']

accepted_date_formats = ["%Y-%m-%d", "%Y-%m", "%Y", "%Y-%m-%dT%H:%M:%S", "%Y-%m-%dT%H:%M","%Y-%Y"]

template_dict = {
    "native": {
        "project_alias_key": "alias",
        "exp_alias_key": "alias",
        "sample_alias_key": "alias",
        "valid_athena_sheet_names": ["COLLABORATORS","project","sample","experiment","run"],
        "ena_checklist_key": "ENA-CHECKLIST",
        "center_name_key": "institute_name",
        "study_type_key": "study_type",
        "nb_row_to_skip": 0,
        "EXPERIMENT_REF": "EXPERIMENT_REF",
        "filename_r1": "filename_r1",
        "filename_r2": "filename_r2",
        "data_dir": "data_dir",
        "INSTRUMENT": "INSTRUMENT",
        "SAMPLE_REF": "SAMPLE_DESCRIPTOR",
        "submission_status_key": "submission_status",
        "submission_status_yes": "YES",
        "mandatory_all_field_lst": ["sample_name","project name","title","sample_description","environmental_sample","investigation type","ENA-CHECKLIST","environmental package","scientific_name","common_name","taxon_id" ,"collection_date","isolation_source","geographic location (country and/or sea)","geographic location (region and locality)","geographic location (depth)","geographic location (elevation)","geographic location (latitude)","geographic location (longitude)","LIBRARY_LAYOUT","INSTRUMENT","LIBRARY_NAME","LIBRARY_STRATEGY","LIBRARY_SOURCE","LIBRARY_SELECTION","PLATFORM","FILETYPE","sequencing method","filename_r1","data_dir"]
    },
    "egide_indiv": {
        "project_alias_key": "ALIAS",
        "exp_alias_key": "alias",
        "sample_alias_key": "alias",
        "valid_athena_sheet_names": ['STUDY', 'COLLABORATORS', 'SAMPLES_general', 'SAMPLES_indiv', 'SAMPLES_chemphys', 'SEQUENCING'],
        "ena_checklist_key": "sample_checklist",
        "center_name_key": "CENTER_NAME",
        "study_type_key": "STUDY_TYPE",
        "nb_row_to_skip": 3,
        "EXPERIMENT_REF": "alias",
        "filename_r1": "RAW_FILENAME_R1",
        "filename_r2": "RAW_FILENAME_R2",
        "data_dir": "DATA_DIRECTORY",
        "INSTRUMENT": "INSTRUMENT_MODEL",
        "SAMPLE_REF": "sample_ref",
        "submission_status_key": "SUBMISSION_STATUS",
        "submission_status_yes": "YES",
        "mandatory_all_field_lst": ["sample_name","project name","title","sample_description","investigation type","sample_checklist","scientific_name","common_name","taxon_id" ,"collection_date","isolation_source","geographic location (country and/or sea)","geographic location (region and locality)","geographic location (depth)","geographic location (elevation)","geographic location (latitude)","geographic location (longitude)","LIBRARY_LAYOUT","INSTRUMENT_MODEL","LIBRARY_NAME","LIBRARY_STRATEGY","LIBRARY_SOURCE","LIBRARY_SELECTION","PLATFORM","FILETYPE","sequencing method","RAW_FILENAME_R1","DATA_DIRECTORY"]
    },
    "egide_meta": {
        "project_alias_key": "ALIAS",
        "exp_alias_key": "alias",
        "sample_alias_key": "alias",
        "valid_athena_sheet_names": ['STUDY', 'COLLABORATORS', 'SAMPLES_general', 'SAMPLES_indiv', 'SAMPLES_meta', 'SAMPLES_chemphys', 'SEQUENCING'],
        "ena_checklist_key": "sample_checklist",
        "center_name_key": "CENTER_NAME",
        "study_type_key": "STUDY_TYPE",
        "nb_row_to_skip": 3,
        "EXPERIMENT_REF": "alias",
        "filename_r1": "RAW_FILENAME_R1",
        "filename_r2": "RAW_FILENAME_R2",
        "data_dir": "DATA_DIRECTORY",
        "INSTRUMENT": "INSTRUMENT_MODEL",
        "SAMPLE_REF": "sample_ref",
        "submission_status_key": "SUBMISSION_STATUS",
        "submission_status_yes": "YES",
        "mandatory_all_field_lst": ["sample_name","project name","title","sample_description","environmental_sample","sample_checklist","scientific_name","common_name","taxon_id" ,"collection_date","isolation_source","geographic location (country and/or sea)","geographic location (region and locality)","geographic location (depth)","geographic location (elevation)","geographic location (latitude)","geographic location (longitude)","LIBRARY_LAYOUT","INSTRUMENT_MODEL","LIBRARY_NAME","LIBRARY_STRATEGY","LIBRARY_SOURCE","LIBRARY_SELECTION","PLATFORM","FILETYPE","sequencing method","RAW_FILENAME_R1","DATA_DIRECTORY"]
    }
}

sheet_names_dict = {
    "native": {
        "project": ["project"],
        "sample": ["sample"],
        "experiment": ["experiment"],
        "run": ["run"]
    },
    "egide_indiv": {
        "project": ["STUDY"],
        "sample": ["SAMPLES_general","SAMPLES_indiv","SAMPLES_chemphys"],
        "experiment": ["SEQUENCING"],
        "run": ["SEQUENCING"] 
    },
    "egide_meta": {
        "project": ["STUDY"],
        "sample": ["SAMPLES_general","SAMPLES_indiv","SAMPLES_meta","SAMPLES_chemphys"],
        "experiment": ["SEQUENCING"],
        "run": ["SEQUENCING"]
    }
}

project_mandatory_attributes = {
    "native": {
        'institute_name':'INSTITUTE_NAME',
        'center_project_name':'CENTER_PROJECT_NAME',
        'study_type':'STUDY_TYPE',
        'study_abstract':'STUDY_ABSTRACT'
    },
    "egide_indiv": {
        'CENTER_NAME':'INSTITUTE_NAME',
        'CENTER_PROJECT_NAME':'CENTER_PROJECT_NAME',
        'STUDY_TYPE':'STUDY_TYPE',
        'STUDY_ABSTRACT':'STUDY_ABSTRACT'
    },
    "egide_meta": {
        'CENTER_NAME':'INSTITUTE_NAME',
        'CENTER_PROJECT_NAME':'CENTER_PROJECT_NAME',
        'STUDY_TYPE':'STUDY_TYPE',
        'STUDY_ABSTRACT':'STUDY_ABSTRACT'
    }
}

project_header_attributes = {
    "native": {
        'name':'NAME',
        'title':'TITLE',
        'study_description':'DESCRIPTION'
    },
    "egide_indiv": {
        'NAME':'NAME',
        'STUDY_TITLE':'TITLE',
        'STUDY_DESCRIPTION':'DESCRIPTION'
    },
    "egide_meta": {
        'NAME':'NAME',
        'STUDY_TITLE':'TITLE',
        'STUDY_DESCRIPTION':'DESCRIPTION'
    }
}
sample_taxonomic_attributes = {'taxon_id':'TAXON_ID',
                               'scientific_name':'SCIENTIFIC_NAME',
                               'common_name':'COMMON_NAME'}
non_writable_attributes = ["release_date", "RELEASE_DATE", "alias", "ALIAS", "collection date"]

# List of attributes that must contain float values
#float_field_lst = ["geographic location (depth)","geographic location (elevation)","alkalinity","barometric pressure","carbon dioxyde","conductivity" ,"fluorescence" ,"humidity","light intensity" ,"nitrate","pH" ,"phosphate","pollutants","pressure" ,"salinity","sample salinity","sample temperature","sodium","solar irradiance","temperature","total depth of water column" ,"total nitrogen","water current","wind speed"]
float_field_lst = ["geographic location (elevation)","alkalinity","barometric pressure","carbon dioxyde","conductivity" ,"fluorescence" ,"humidity","light intensity" ,"nitrate","pH" ,"phosphate","pollutants","pressure" ,"salinity","sample salinity","sample temperature","sodium","solar irradiance","temperature","total depth of water column" ,"total nitrogen","water current","wind speed"]

# List of attributes that must contain date formatted attributes (YYYY-MM-DD)
#date_field_lst = ["collection_date","release_date"]
date_field_lst = ["collection_date"]

# List of attributes that must contain integer values
#int_field_lst = ["NOMINAL_LENGTH","INSERT_SIZE"]
#int_field_lst = ["NOMINAL_LENGTH","READ_LENGTH"]
int_field_lst = ["READ_LENGTH"]

meta_terms = ["Metagenomics"]
#meta_terms = ["nabla"]

# List of mandatory fields for all checklists
#mandatory_all_field_lst = ["name","title","study_description","study_abstract","study_type","institute_name","center_project_name","release_date","sample_name","project name","title","sample_description","environmental_sample","investigation type","ENA_CHECKLIST","environmental package","scientific_name","common_name","taxon_id" ,"collection_date","isolation_source","geographic location (country and/or sea)","geographic location (region and locality)","geographic location (depth)","geographic location (elevation)","geographic location (latitude)","geographic location (longitude)","LIBRARY_LAYOUT","INSTRUMENT","LIBRARY_NAME","LIBRARY_STRATEGY","LIBRARY_SOURCE","LIBRARY_SELECTION","NOMINAL_LENGTH","PLATFORM","FILETYPE","pcr primers","multiplex identifiers","adapters","sequencing method","filename_r1","data_dir"]
#mandatory_all_field_lst = ["sample_name","project name","title","sample_description","environmental_sample","investigation type","ENA-CHECKLIST","environmental package","scientific_name","common_name","taxon_id" ,"collection_date","isolation_source","geographic location (country and/or sea)","geographic location (region and locality)","geographic location (depth)","geographic location (elevation)","geographic location (latitude)","geographic location (longitude)","LIBRARY_LAYOUT","INSTRUMENT","LIBRARY_NAME","LIBRARY_STRATEGY","LIBRARY_SOURCE","LIBRARY_SELECTION","NOMINAL_LENGTH","PLATFORM","FILETYPE","pcr primers","multiplex identifiers","adapters","sequencing method","filename_r1","data_dir"]
mandatory_all_field_lst = ["sample_name","project name","title","sample_description","environmental_sample","investigation type","ENA-CHECKLIST","environmental package","scientific_name","common_name","taxon_id" ,"collection_date","isolation_source","geographic location (country and/or sea)","geographic location (region and locality)","geographic location (depth)","geographic location (elevation)","geographic location (latitude)","geographic location (longitude)","LIBRARY_LAYOUT","INSTRUMENT","LIBRARY_NAME","LIBRARY_STRATEGY","LIBRARY_SOURCE","LIBRARY_SELECTION","PLATFORM","FILETYPE","sequencing method","filename_r1","data_dir"]

# List of **joker** terms for mandatory fields
joker = ["not provided","not collected", "not applicable", "missing: control sample", "missing"]


# List of checklists which must contain meta sheet related attributes
#meta_chk_lst = ["ERC000050","ERC000036","ERC000013","ERC000025","ERC000021","ERC000022","ERC000023","ERC000024"]
meta_chk_lst = ["ERC000036","ERC000013","ERC000025","ERC000021","ERC000022","ERC000023","ERC000024"]


checklist_specificity = {
    'ERC000024': { "geographic location (depth)": "depth" },
    'ERC000036': { "environment (feature)": "sewage type" }
}

checklists_mandatory_attributes = {
    'ERC000039': ["isolate","dev_stage","collecting institution","collector name"],
    'ERC000043': ["strain"],
    'meta': ["environment (biome)", "environment (feature)", "environment (material)", "target gene", "target subfragment"]
}
# Dictionary of possible unit for float attributes
default_unit = {
"geographic location (depth)":"m",
"geographic location (elevation)":"m",
"geographic location (latitude)":"DD",
"geographic location (longitude)":"DD",
"conductivity":"mS/cm",
"fluorescence":"V",
"light intensity":"lux",
"nitrate":"µmol/L",
"phosphate":"µmol/L",
"pressure":"bar",
"salinity":"psu",
"sodium":"µmol/L",
"temperature":"ºC",
"total depth of water column":"m",
"total nitrogen":"µmol/L",
"water current":"m3/s",
"alkanility":"mEq/L",
"barometric pressure":"hPa",
"carbon dioxyde":"µmol/L",
"humidity":"%",
"pollutants":"M/L",
"sample salinity":"psu",
"sample temperature":"psu",
"solar irradiance":"W/m2",
"wind speed":"m/s"
}

possible_unit = {
"geographic location (depth)":["m"],
"geographic location (elevation)":["m"],
"geographic location (latitude)":["DD"],
"geographic location (longitude)":["DD"],
"conductivity":["mS/cm"],
"fluorescence":["V","mg Chla/m3"],
"light intensity":["lux"],
"nitrate":["µmol/L"],
"phosphate":["µmol/L"],
"pressure":["bar","atm"],
"salinity":["psu"],
"sodium":["parts/million","µmol/L"],
"temperature":["ºC"],
"total depth of water column":["m"],
"total nitrogen":["µmol/L","µg/L"],
"water current":["m3/s","knot"],
"alkanility":["mEq/L"],
"barometric pressure":["Torr","in. Hg","hPa","mm Hg"],
"carbon dioxyde":["µmol/L"],
"humidity":["%","g/m3"],
"pollutants":["M/L","g","mg/L"],
"sample salinity":["psu"],
"sample temperature":["psu"],
"solar irradiance":["W/m2"],
"wind speed":["k/h","m/s"]
}

# List of sheets from file
sheet_list = ["Instructions", "STUDY", "COLLABORATORS", "SAMPLES_general", "SAMPLES_meta", "SAMPLES_indiv", "sample", "SEQUENCING", "SAMPLES_chemphys", "project", "experiment", "run", "lists"]
native_sheet_list = ["Instructions", "STUDY", "COLLABORATORS", "SAMPLES_general", "SAMPLES_meta", "SAMPLES_indiv", "sample", "SEQUENCING", "SAMPLES_chemphys", "project", "experiment", "run", "lists"]
egide_sheet_list = ["Instructions", "STUDY", "COLLABORATORS", "SAMPLES_general","SEQUENCING"]

exp_title_key = "TITLE"
experiment_mandatory_values = ["LIBRARY_NAME", "LIBRARY_STRATEGY", "LIBRARY_SOURCE", "LIBRARY_SELECTION",
                               "LIBRARY_LAYOUT", "PLATFORM", "INSTRUMENT_MODEL", exp_title_key, "STUDY_REF", "INSTRUMENT", "SAMPLE_DESCRIPTOR"]
experiment_special_keys = ["NOMINAL_LENGTH", "SUBMISSION_STATUS", "RAW_FILENAME_R1", "RAW_FILENAME_R2", "DATA_DIRECTORY", "sample_ref", "FILETYPE"]

is_submittable_value = "submittable"


separators = [",", ";", "|"]

useless_values = {
"environmental_sample":["no","No"],
"environmental package":["none","not provided", "None"],
"geographic location (elevation)":["not provided","not applicable"],
"geographic location (depth)":["not provided","not applicable"]
}
