#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------------------------#
# Python script to generate XML files to submit to ENA server					#
#
# part of athENA pipeline : tool for automatic submission of sequencing data and metadata to ENA#
# Original script : https://github.com/bigey/ena-submit/blob/master/generate-xml.py             #
# Current version : 2022 Pauline Auffret                                                        #
# -----------------------------------------------------------------------------------------------#
import os
import sys
import argparse
import hashlib

from lxml.builder import unicode
from yattag import Doc, indent
# from pyexcel_ods3 import get_data
from pyexcel_ods import get_data
from datetime import datetime
import os.path
from os import path
import re
import unicodedata
import xlrd
from loguru import logger
from pathlib import Path
import requests
# xlrd.xlsx.ensure_elementtree_imported(False, None)
# xlrd.xlsx.Element_has_iter = True
from config import *

# Print help message


def set_up_argparse():
    parser = argparse.ArgumentParser(
        description="""
        Convert Excel or ODS spreadsheet file to xml files formated for ENA submission server
        """)
    parser.add_argument("--input_file", "-i",
                        metavar="INPUT_FILE", required=True,
                        help="spreadsheet file (xls* or ods format)")

    parser.add_argument("--md5_file", "-l",
                        dest="md5_file", required=True,
                        help="Absolute path to md5sum file of files to upload to ENA (md5sum file format)")

    parser.add_argument("--path_prefix", "-w", type=str, default="/",
                        dest="path_prefix",
                        help="Path prefix for files listed in list of files to upload (-f parameter)")

    parser.add_argument("--output_dir", "-o",
                        dest="output_dir", default=os.getcwd(),
                        help="output directory for xml files. Default: current dir")

    parser.add_argument("--ignore_sub_status", "-x",
                        dest="ignoreSubStatus", default=False, type=str,
                        help="Print all records in XML files regardless of submission status (true/false)")

    parser.add_argument("--is_project_converted", "-p",
                        dest="is_project_converted", default="True", type=str,
                        help="Is the project sheet should be converted to xml")

    parser.add_argument("--is_sample_converted", "-s",
                        dest="is_sample_converted", default="True", type=str,
                        help="Is the sample sheet should be converted to xml")

    parser.add_argument("--is_experiment_converted", "-e",
                        dest="is_experiment_converted", default="True", type=str,
                        help="Is the experiment sheet should be converted to xml")

    parser.add_argument("--is_run_converted", "-r",
                        dest="is_run_converted", default="True", type=str,
                        help="Is the run sheet should be converted to xml")

    parser.add_argument("--only_extract_filenames", "-y",
                        dest="only_names", default="False", type=str,
                        help="Do not output XML, just list of file names to upload.")

    parser.add_argument("--is_brokering_submission", "-b",
                        dest="isBrokering", default="True", type=str,
                        help="Is the submission done using a regular ENA user account, or a ENA broker account? (true/false)")

    parser.add_argument("--max_project", "-m",
                        dest="maxp", type=int, default=1,
                        help="Maximum number of projects to be handled within this submission. Default: 1")

    parser.add_argument("--data_file_name", "-f",
                        dest="upfile", default="files-to-upload.txt",
                        help="Name for text file to write list of files to upload")

    parser.add_argument("--upstream_data_file_name", "-u",
                        dest="upupfile", default="files-to-md5.txt",
                        help="Name for text file to write list of files to compute md5sum (only used with --only_extract_filenames True)")

    parser.add_argument("--trace_file", "-t",
                        dest="trace", default="trace.log",
                        help="Logger outputs for TRACE level only")



    opts = parser.parse_args()


    return opts


def log(*args, **kwargs) -> None:
    print(*args, file=sys.stderr, **kwargs)


def is_data_row(row=list) -> bool:
    """
     Checks if a row extracted from an sheet actually contains data or is an empty or containing irrelevant data

     :param row: The input row as list.
     :returns: True/False.
     :rtype: Boolean.
     """
    try:
        # Check if the length of row is greater than 0
        if len(row) > 0:
            # Check if the first element of row is valid i.e. indicating that the row actually contains meaning data
            if str(row[0]) not in invalid_first_cell_content:
                return True
        return False
    except Exception as e:
        # Log any unexpected errors
        logger.error(f"An error occurred: {e}")
        return False


def is_valid_data_value(value=str) -> bool:
    """
     Checks if a row extracted from an sheet actually contains data or is an empty or containing irrelevant data

     :param value: The input value as string.
     :returns: True/False.
     :rtype: Boolean.
     """
    try:
        if value == 0:
            logger.trace(
                f"Value '{value}' is 0: are you sure this is a real value ?")
            return 0  # Explicitly returning 0 for integer 0
        if value == "":
            logger.trace(
                f"Value '{value}' is empty")
            return False  # Explicitly returning 0 for integer 0
        # Check if the length of value is greater than 0
        if len(str(value)) > 0 and str(value).strip() not in joker and str(value) != "0" and value != 0:
            return True
        elif str(value) == "0":
            logger.trace(
                f"Value '{value}' is 0: are you sure this is a real value ?")
            return 0
        elif str(value) == "":
            return 0
        #elif value in joker:
        #    logger.info(
        #        f"Value '{value}' is a joker value.")
        #    return("joker")
        else:
            logger.trace(
                f"Value '{value}' is not valid or is a joker value: format cannot be checked.")
            return False
    except Exception as e:
        # Log any unexpected errors
        logger.error(
            f"An error occurred during the evaluation of {value} data value: {e}")
        return False

# Three functions to collect data from sheet and store them into dictionary
#def excel_sheet_to_dict(data: dict, sheet=str, nb_row_to_skip=0) -> dict:
#    """
#      Converts one Excel sheet content to a dictionary
#
#      :param sheet: Input Excel sheet name (string)
#      :param data: Input Excel file as pyexcel_ods3 object **without header** i.e. first row must be column names
#      :returns: sheet_dict
#      :rtype: Dictionary
#      """
#    try:
#        sheet_dict = {}
       # Check if the specified sheet exists in the data
#        if sheet not in data:
#            raise KeyError(f"The sheet '{sheet}' does not exist in the data.")

        # Get the keys from the first row of the sheet and remove it from the data
#        keys = data[sheet][nb_row_to_skip]
#        print(keys)
        # For all rows in sheet
#        for row in data[sheet][nb_row_to_skip+1::]:
            # Check if row contains valid data
#            if is_data_row(row):
                # Create a dictionary from the keys and the current row
#                row_dict = dict(zip(keys, row))
                # If the current sheet is collaborators sheet and sheet contains the key key_collab_name
#                if sheet == collaborators_sheet_name.strip():
#                    if key_collab_name in row_dict:
                        # Add the row dictionary to final dictionary sheet_dict using the key_collab_name as the key
#                       # For example sheet_dict['AUFFRET']= {"First Name":"Pauline";"Last Name":"AUFFRET"...}
#                       sheet_dict[row_dict[key_collab_name]] = row_dict
#                    else:
                        # If key_collab_name key is missing, raise a KeyError
#                        raise KeyError(
#                            f"The {key_collab_name} column is missing in data sheet {sheet}.")
#                else:
#                    for valid_alias_key in key_row_name:
#                        if valid_alias_key in row_dict:
                            # Add the row dictionary to final dictionary sheet_dict using the key_row_name as the key
                            # For example sheet_dict['sample_02']= {"sample_name":"sample_02";"collection_date":"2024-05-23"...}
#                            sheet_dict[row_dict[valid_alias_key]] = row_dict
#                            is_alias = True
#                    if not is_alias:
                        # If 'alias' key is missing, raise a KeyError
#                        raise KeyError(f"The {key_row_name} column is missing in the data.")
#        return sheet_dict
#    except KeyError as ke:
        # Log and return the KeyError
#        logger.error(
#            f"{ke}, sorry. Exit.")
#        exit(1)
#    except Exception as e:
        # Log and return any other unexpected errors
#        logger.error(f"An error occurred: {e}")
#        exit(1)




#def excel_sheet_to_dict(data: dict, sheets=list, nb_row_to_skip=0) -> dict:
#    """
#    Converts multiple Excel sheet contents to a single dictionary.

#    :param sheets: List of sheet names (strings) to process.
#    :param data: Input Excel file as pyexcel_ods3 object **without header**, i.e., first row must be column names.
#    :param nb_row_to_skip: Number of rows to skip at the start of each sheet.
#    :returns: Combined dictionary of all sheets.
#    :rtype: Dictionary
#    """
#    try:
#        combined_sheet_dict = {}  # Final dictionary to store combined data from all sheets

#        for sheet in sheets:
#            print(sheet)
            # Check if the specified sheet exists in the data
#            if sheet not in data:
#                raise KeyError(f"The sheet '{sheet}' does not exist in the data.")

            # Get the keys from the first row of the sheet and remove it from the data
#            keys = data[sheet][nb_row_to_skip]
#            print(keys)
            # For all rows in the sheet
#            for row in data[sheet][nb_row_to_skip + 1::]:
                # Check if row contains valid data
#                if is_data_row(row):
                    # Create a dictionary from the keys and the current row
#                    row_dict = dict(zip(keys, row))
                    # Determine the unique key for the row
#                    unique_key = None
#                    if sheet == collaborators_sheet_name.strip():
#                        if key_collab_name in row_dict:
#                            unique_key = row_dict[key_collab_name]
#                        else:
#                            raise KeyError(
#                                f"The {key_collab_name} column is missing in data sheet {sheet}.")
#                    else:
#                        for valid_alias_key in key_row_name:
#                            if valid_alias_key in row_dict:
#                                unique_key = row_dict[valid_alias_key]
#                                break
#                        if unique_key is None:
#                            raise KeyError(f"The {key_row_name} column is missing in the data.")

                    # Add the row dictionary to the final combined dictionary
#                    if not unique_key in combined_sheet_dict:
                        #raise KeyError(f"Duplicate key '{unique_key}' found across sheets.")
#                        combined_sheet_dict[unique_key] = row_dict
#        return combined_sheet_dict

#    except KeyError as ke:
        # Log and return the KeyError
#        logger.error(f"{ke}, sorry. Exit.")
#        exit(1)
#    except Exception as e:
        # Log and return any other unexpected errors
#        logger.error(f"An error occurred: {e}")
#        exit(1)

def excel_sheet_to_dict(data: dict, sheets=list, nb_row_to_skip=0) -> dict:
    """
    Converts multiple Excel sheet contents to a single dictionary, merging rows based on a unique key.

    :param sheets: List of sheet names (strings) to process.
    :param data: Input Excel file as pyexcel_ods3 object **without header**, i.e., first row must be column names.
    :param nb_row_to_skip: Number of rows to skip at the start of each sheet.
    :returns: Combined dictionary of all sheets.
    :rtype: Dictionary
    """
    try:
        combined_sheet_dict = {}  # Final dictionary to store combined data from all sheets

        for sheet in sheets:
            logger.info(f"Processing sheet: {sheet}")
            # Check if the specified sheet exists in the data
            if sheet not in data:
                raise KeyError(f"The sheet '{sheet}' does not exist in the data.")

            # Get the keys from the first row of the sheet
            keys = data[sheet][nb_row_to_skip]
            logger.trace(f"Keys for sheet {sheet}: {keys}")

            # Process each row in the sheet
            for row in data[sheet][nb_row_to_skip + 1:]:
                if is_data_row(row):  # Ensure row contains valid data
                    row_dict = dict(zip(keys, row))  # Map keys to the row's values

                    # Determine a unique key for the row
                    unique_key = None
                    # key_collab_name = "Last name" -> indicates we're in COLLABORATORS sheet
                    if key_collab_name in row_dict:
                        unique_key = row_dict[key_collab_name]
                    else:
                        # key_row_names = ["ALIAS","alias"]
                        for valid_alias_key in key_row_name:
                            if valid_alias_key in row_dict:
                                unique_key = row_dict[valid_alias_key]
                                break
                        if unique_key is None:
                            raise KeyError(f"The alias column ({key_row_name}) is missing in sheet {sheet}.")

                    # Check if the key already exists in the combined dictionary
                    if unique_key in combined_sheet_dict:
                        # Merge existing data with new data
                        combined_sheet_dict[unique_key].update(row_dict)
                    else:
                        # Initialize new entry in the combined dictionary
                        combined_sheet_dict[unique_key] = row_dict

        return combined_sheet_dict

    except KeyError as ke:
        logger.error(f"{ke}, sorry. Exit.")
        exit(1)
    except Exception as e:
        logger.error(f"An error occurred: {e}")
        exit(1)


def attributes_to_dict(data: dict, sheet=str, nb_row_to_skip=0) -> list:
    """
      Gets all attributes (i.e. column names in one Excel sheet) into a list

      :param sheet: Input Excel sheet name (string)
      :param data: Input Excel file as pyexcel_ods3 object **without header** i.e. first row must be column names
      :returns: attributes
      :rtype: list
    """
    try:
        # Check if the specified sheet exists in the data
        if sheet not in data:
            raise KeyError(f"The sheet '{sheet}' does not exist in the data.")
        return data[sheet][nb_row_to_skip]
    except Exception as e:
        # Log and return any other unexpected errors
        logger.error(f"An error occurred: {e}")
        exit(1)

def strip_accents(text) -> str:
    """
    Strip accents from input String.

    :param text: The input string.
    :type text: String.

    :returns: The processed String.
    :rtype: String.
    """
    try:
        text = unicode(text, 'utf-8')
    except (TypeError, NameError):  # unicode is a default on python 3
        pass
    text = unicodedata.normalize('NFD', str(text))
    text = text.encode('ascii', 'ignore')
    text = text.decode("utf-8")
    text = text.strip()
    return str(text)


# Function to check if joker values are correctly labelled
def control_missing_values(value=str) -> str:
    """
      Controls missing values format and convert them if needed into valid missing values for ENA validation.
      :param value: Input value as string

      :returns: correct_value
      :rtype: string
    """
    if str(value).upper() in wrong_missing_values:
        logger.trace(
            f"Replace missing value '{value}' by '{correct_missing_value}")
        correct_value = correct_missing_value
    else:
        correct_value = value
    return correct_value

# Function to check if a file exists
def check_file_exists(filepath: str, file_description: str) -> None:
    """
      Checks if input file exist...
      :param filepath: Input value as string
      :param file_description:
    """
    try:
        if not os.path.exists(filepath):
            raise FileNotFoundError(
                f"The {file_description} ({filepath}) does not exist!")
    except FileNotFoundError as e:
        logger.error(f"An error has occurred : {e}")

# Function to create directory if not existing
def make_dir_if_not_exist(dir_name: str) -> None:
    """
      Create a directory if not existing
      :param dir_name: New directory name (string)
    """
    try:
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
    except Exception as e:
        logger.error(
            f"An error occurred while creating the directory '{dir_name}': {e}")

# Function to get a list of all fields contained in the 4 tabs "study", "sample", "experiment", "run"
def get_attribute_list(data: dict, template_type: str) -> list:
    """
      Gets all attributes from an athENA Excel file into a list
      :param data: Input Excel file as data pyexcel_ods3 object

      :returns: attributes
      :rtype: list
    """
    attributes = []
    try:
        for sheet in data:
            if sheet in template_dict[template_type]["valid_athena_sheet_names"]:
                if sheet == "COLLABORATORS":
                    nb_row_to_skip = 0
                else:
                    nb_row_to_skip = template_dict[template_type]["nb_row_to_skip"]
                keys = attributes_to_dict(data, sheet, nb_row_to_skip)
                attributes += keys
                logger.success(
                    f"The attribute list from {sheet} sheet has been successfully retrieved :)")
    except Exception as e:
        logger.error(
            f"An error occurred while processing the data from {data}: {e}")
    return attributes


# Function to check data format (int, float, date)
def check_data_format(attribute: str, value: str) -> bool:
    """
       Checks if given value has correct format
       :param attribute: Input attribute name as string
       :param value: Input attribute value

       :returns: True/False
       :rtype: Boolean
     """
    try:
        if is_valid_data_value(value) is False:
            return False
        if is_valid_data_value(value) == 0:
            logger.trace(f"Value is 0 : is it a real 0, or an empty cell ?")
            return 0
        if attribute in float_field_lst and not re.match(r'^\d+(\.\d+)?$', str(value)):
            raise TypeError(
                f"Incorrect data format for attribute {attribute}: must be float but value {value} is provided. Exit.")
        elif attribute in int_field_lst and not re.match(r'^\d+$', str(value)):
            raise TypeError(
                f"Incorrect data format for attribute {attribute}: must be integer but value {value} is provided. Exit.")
        elif attribute in date_field_lst:
            if re.match(r'^\d{4}\/\d{4}$', str(value)) :  # Format "2006/2012"
                logger.trace(f"Accepted interval format for {attribute}: {value}. However, it would be better to use a more specific date format (e.g., YYYY-MM-DD or YYYY-MM).")
                return True  
            for date_format in accepted_date_formats:
                try:
                    datetime.strptime(str(value), date_format)
                    break
                except ValueError:
                    continue
            else:
                logger.error(
                    f"Incorrect data format for attribute {attribute}: must comply with ISO 8601 (YYYY-MM-DD ; YYYY-MM ; YYYY-MM-DDThh:mm:ss for example ; but value {value} is provided. Exit.")
                sys.exit(1)
        return True
    except Exception as e:
        logger.error(
            f"An error occurred while validating '{value}' value format for '{attribute}' attribute: {e}")
        return False

def compare_string_without_case(chaine1: str, chaine2: str) -> bool:
    return chaine1.lower() == chaine2.lower()


def deduplicate_list(attributes=list) -> list:
    """
       Removes duplicates values from a list of attributes
       :param attributes: List of attributes

       :returns: deduplicated_attributes
       :rtype: list
     """
    try:
        if attributes is None:
            raise ValueError("Attribute list cannot be empty.")
        if not isinstance(attributes, list):
            raise TypeError("Attributes are not given as a list")
        deduplicated_attributes = []
        for attribute in attributes:
            if attributes not in deduplicated_attributes:
                deduplicated_attributes.append(attribute)
        return deduplicated_attributes
    except Exception as e:
        logger.error(f"An error has occurred : {e}")
        return None

def handle_checklist_specificity(attributes: list, checklist_id: str) -> list:
    """
    Replace attribute values in the list based on the checklist specificity mapping.

    :param attributes: List of attribute names (list)
    :param checklist_id: Checklist identifier (str)
    :return: New list of attributes with replacements based on the checklist specificity (list)
    """
    try:
        # Check if the checklist_id is in the checklist_specificity dictionary
        if checklist_id in checklist_specificity:
            # Get checklist_id dictionary
            specificity_mapping = checklist_specificity[checklist_id]
            logger.trace(f"Found checklist specificity for ID '{checklist_id}': {specificity_mapping}")
        
            # Replace values in attributes according to specificity_mapping
            updated_attributes = [specificity_mapping.get(attr, attr) for attr in attributes]
            return updated_attributes
        else:
            # If checklist_id not present in checklist_specificity dictionary, return the original attribute list
            return attributes
    except Exception as e:
        logger.error(f"An error has occurred : {e}")
        return None



def get_mandatory_attributes(checklist: str, study_type: str, template_type: str) -> list:
    """
       Gets all mandatory attributes for a given ENA checklist
       :param checklist: ENA checklist identifier (e.g. ERC000043)
       :param study_type: study type from study sheet as string (e.g. Metagenomics)

       :returns: deduplicated mandatory attribute list
       :rtype: list
     """
    try:
        mandatory_attributes = template_dict[template_type]["mandatory_all_field_lst"].copy(
        )
        if checklist in checklists_mandatory_attributes:
            mandatory_attributes.extend(
                checklists_mandatory_attributes[checklist])
        if is_meta_project(study_type, checklist):
            mandatory_attributes.extend(
                checklists_mandatory_attributes['meta'])
        return deduplicate_list(mandatory_attributes)
    except KeyError as ke:
        logger.error(
            f"Invalid key was used to search in 'checklists_mandatory_attributes' list : {ke}")
        return None
    except Exception as e:
        logger.error(f"n error has occurred : {e}")
        return None


def is_compliant(checklist=str, attributes=list, study_type=str, template_type=str) -> bool:
    """
       Checks if all mandatory attributes are present in attributes for a given checklist
       :param checklist: ENA checklist identifier (e.g. ERC000043)
       :param attributes: list of attributes
       :param study_type: study type from study sheet as string (e.g. Metagenomics)

       :returns: True/False
       :rtype: Boolean
     """
    try:
        mandatory_attributes = get_mandatory_attributes(checklist, study_type, template_type)
        mandatory_attributes = handle_checklist_specificity(mandatory_attributes, checklist)
        if attributes is None:
            raise ValueError("Attribute list cannot be empty.")
        if not isinstance(attributes, list):
            raise TypeError("Attributes are not given as a list")
        for attribute in mandatory_attributes:
            if attribute not in attributes:
                error_msg = f"Missing mandatory field '{attribute}' in provided attributes for checklist {checklist}. Exit."
                raise ValueError(error_msg)
        logger.success("All mandatory fields are present :)")
        return True
    except Exception as e:
        logger.error(f"An error has occurred : {e}")
        exit(1)


# Function to create list of collaborators
def get_authors(authors: dict) -> str:
    try:
        author_list = [author['First name']+" "+author['Last name']
                       for author in authors.values()]
        author_string = ", ".join(author_list)
        last_comma_index = author_string.rfind(',')
        if last_comma_index != -1:
            author_string = author_string[:last_comma_index] + \
                ' and' + author_string[last_comma_index + 1:]
        return author_string
    except KeyError as ke:
        logger.error(f"Missing field '{ke.args[0]}' in attributes.")
        exit(1)
    except Exception as e:
        logger.error(f"An error has occurred : {e}")
        exit(1)


# Function to get checklist
# Consider putting checklist value in STUDY description ?
#def get_checklist(samples: dict, template_type: str) -> list:
#    checklists = []
#    nb_samples = 0
#    try:
#        for row_sample in samples:
#            sample = samples[row_sample]
#            checklist = sample[template_dict[template_type]["ena_checklist_key"]]
#            print(checklist)
#            nb_samples += 1
#            if nb_samples > 1 and checklist not in checklists:
#                raise ValueError(
#                    "There are more than 1 checklist used for sample description, this is not permitted by ENA")
#            checklists.append(checklist)
#        return checklist
#    except KeyError as ke:
#        logger.error(f"Missing field '{ke.args[0]}' in attributes.")
#        exit(1)
#    except Exception as e:
#        logger.error(f"An error has occurred : {e}")
#        exit(1)

# Function to get checklist
# Consider putting checklist value in STUDY description ?
def get_checklist(samples: dict, template_type: str) -> list:
    checklists = set()  # Utilisation de set pour éviter les doublons
    checklist = None  # Initialisation de checklist par défaut
    nb_samples = 0

    try:
        for row_sample in samples:
            sample = samples[row_sample]
            checklist = sample[template_dict[template_type]["ena_checklist_key"]]
            nb_samples += 1

            if nb_samples > 1 and checklist not in checklists:
                raise ValueError(
                    "There are more than 1 checklist used for sample description, this is not permitted by ENA"
                )
            checklists.add(checklist)  # Ajouter à l'ensemble des checklists

        return checklist  # Retourner la dernière checklist trouvée

    except KeyError as ke:
        logger.error(f"Missing field '{ke.args[0]}' in attributes.")
        exit(1)
    except Exception as e:
        logger.error(f"An error has occurred: {e}")
        exit(1)


# Function to get center_name value (for broker account submissions)
def get_center_name(projects: dict, center_name_key: str) -> str:
    try:
        for a_project in projects:
            project = projects[a_project]
            center_name = project[center_name_key]
        return center_name
    except KeyError as ke:
        logger.error(f"KeyError: {ke}")
        exit(1)
    except Exception as e:
        logger.error(f"An error occurred while getting the center name: {e}")
        exit(1)

# Function to get study type value
def get_study_type(projects: dict, study_type_key: str) -> str:
    try:
        for a_project in projects:
            project = projects[a_project]
            study_type = project[study_type_key]
        return study_type
    except KeyError as ke:
        logger.error(
            f"The following key was not found in the project attributes: {ke}")
        exit(1)
    except Exception as e:
        logger.error(f"An error occurred while getting the study type: {e}")
        exit(1)

# Function to get study alias/accession
def get_study_ref(projects: dict, alias_key: str) -> str:
    try:
        for a_project in projects:
            project = projects[a_project]
            study_ref = project[alias_key]
        return study_ref
    except KeyError as ke:
        logger.error(
            f"The following key was not found in the project attributes: {ke}")
        exit(1)
    except Exception as e:
        logger.error(f"An error occurred while getting the study alias: {e}")
        exit(1)


def is_valid_doi(doi: str) -> bool:
    """
    Checks if a given string is a valid DOI format.

    :param doi: String representing the DOI to be validated.
    :return: True if the DOI is valid, False otherwise.
    """
    # Regular expression pattern for matching DOI format
    #pattern = r'^10\.\d{4,9}/[-._;()/:A-Za-z0-9]+$'
    #pattern = r'^https://doi\.org/10\.\d{4,9}/[-._;()/:A-Za-z0-9]+$'
    pattern = r'^https://doi\.org/10\.\d{4,9}/[-._;()/:A-Za-z0-9]+([\n;]https://doi\.org/10\.\d{4,9}/[-._;()/:A-Za-z0-9]+)*$'
    # Check if the given string matches the pattern
    try:
        if re.match(pattern, doi):
            logger.info(f"The provided DOI {doi} is valid.")
            return True
        else:
            raise ValueError(
                f"The provided DOI {doi} does not respect expected format.")
    except Exception as e:
        logger.error(f"An error has occurred : {e}")
        exit(1)


def is_meta_project(study_type: str, checklist:str) -> bool:
    """
    Checks if a given study_type is a part of meta_terms list.

    Args:
        study_type (str): The study type to check.
        checklist (str): Checklist to check

    Returns:
        bool: True if the study type is in meta_terms, False otherwise.
    """
    if study_type in meta_terms:
        return True
    if checklist in meta_chk_lst:
        return True
    return False


def is_accession_id(project: str) -> str:
    return project.strip().startswith("PRJ")


def add_project_attribute(doc, tag, text, name: str, value: int) -> None:
    with tag("PROJECT_ATTRIBUTE"):
        with tag("TAG"):
            text(name)
        with tag("VALUE"):
            text(strip_accents(control_missing_values(value)))

def add_project_header_attribute(doc, tag, text: str, name: str, value: int) -> None:
    with tag(name):
        text(strip_accents(control_missing_values(value)))


def add_alias_line_to_project(doc, tag, text: str, alias: list, project: dict, isBrokering: bool) -> None:
    if is_accession_id(alias):
        if isBrokering == True:
            doc.attr(accession=alias, center_name=project[center_name_key])
        else:
            doc.attr(accession=alias)
    else:
        if isBrokering == True:
            doc.attr(alias=alias, center_name=project[center_name_key])
        else:
            doc.attr(alias=alias)


def is_data_ok(sheet: list, key: str) -> bool:
    if key not in float_field_lst and key not in int_field_lst and key not in date_field_lst:
        if sheet[key] == 0:
            return False
    return True


# Function to get info from project sheet
def make_project_xml(projects: dict, max_project: int, author_list: list, isBrokering: bool, template_type: str) -> str:
    try:
        doc, tag, text = Doc().tagtext()

        # Initialize nb_project variable to count the number of projects
        nb_project = 0
        # Initialize project_optional_attributes dictionary which will contains optional attributes
        project_optional_attributes = {}
        with tag("PROJECT_SET"):
            for a_project in projects:
                nb_project += 1
                project = projects[a_project]
                # Check attributes format and value conformity
                for key in project:
                    if is_data_ok(project, key):
                        project[key] = control_missing_values(project[key])
                        check_data_format(key, project[key])
                        if key not in project_header_attributes[template_type] and key not in project_mandatory_attributes[template_type] and key not in non_writable_attributes:
                            project_optional_attributes[key] = project[key]
                            if key == "doi":
                                is_valid_doi(str(project[key]))
               # Create XML file structure
                with tag("PROJECT"):
                    add_alias_line_to_project(
                        doc, tag, text, a_project, project, isBrokering)
                    for name in project_header_attributes[template_type]:
                        add_project_header_attribute(
                            doc, tag, text, project_header_attributes[template_type][name], project[name])
                    with tag("SUBMISSION_PROJECT"):
                        doc.stag("SEQUENCING_PROJECT")
                    with tag("PROJECT_ATTRIBUTES"):
                        add_project_attribute(
                            doc, tag, text, "COLLABORATORS", author_list)
                        for name in project_mandatory_attributes[template_type]:
                            add_project_attribute(
                                doc, tag, text, project_mandatory_attributes[template_type][name], project[name])
                        for name in project_optional_attributes:
                            add_project_attribute(
                                doc, tag, text, name, project[name])
       # Check if the number of projects exceeds the requirements
        if int(max_project) != -1 and nb_project > int(max_project):
            log(
                f"The number of projects listed in STUDY tab is {nb_project} which is beyond the given limit of {max_project}")
            sys.exit(1)
        project_xml = indent(doc.getvalue())
        return project_xml
       
    except Exception as e:
        logger.error(f"An error occurred during project XML generation: {e}")
        exit(1)


def get_submission_status(sample: dict, submission_status_key: dict, submission_status_yes: str, ignore_submission_status: bool) -> str:
    try:
        if ignore_submission_status == "True":
            logger.trace(
                f"Ignoring submission status for sample {sample['alias']} according to --ignore_sub_status parameter.")
            return submission_status_yes
        for key in sample:
            if key.casefold() == submission_status_key.casefold():
                return sample[key]
        logger.trace(
            f"No {submission_status_key} attribute was found. Using 'YES' as default ; the sample will be submitted.")
        return submission_status_yes
    except Exception as e:
        logger.error(
            f"An error occurred during retrieval of submission status for sample {sample['alias']}: {e}")
        exit(1)


def add_taxonomic_metadata(doc, tag, text, sample: dict, sample_taxonomic_attributes: dict) -> str:
    tax_info = ["taxid", "sci_name", "com_name"]
    try:
        for attribute in sample_taxonomic_attributes:
            with tag(sample_taxonomic_attributes[attribute]):
                text(sample[attribute])
            if attribute == "taxon_id":
                tax_info[0] = sample[attribute]
            if attribute == "scientific_name":
                tax_info[1] = sample[attribute]
            if attribute == "common_name":
                tax_info[2] = sample[attribute]
        return tax_info
    except Exception as e:
        logger.error(
            f"An error occurred during the retrieval of taxonomic metadata from sample {sample}: {e}")
        exit(1)


def get_json_data_from_curl(curl_command: str, sci_name: str) -> str:
    try:
        response = requests.get(curl_command)
        if response.status_code == 200 and response.json() != []:
            return response.json()
        else:
            logger.error(
                f"The ENA api does not return any valid taxonomy associated with scientific name '{sci_name}'. Check  https://ena-docs.readthedocs.io/en/latest/faq/taxonomy.html.")
            exit(1)
    except Exception as e:
        logger.error(
            f"Request to url: {curl_command} failed with status code {response.status_code}: {e}")
        exit(1)


def is_submittable_tax(sample, json_data: list, sci_name: str, is_submittable_value: bool) -> bool:
    try:
        if json_data and isinstance(json_data[0], dict):
            if json_data[0].get(is_submittable_value, None) == "true":
                return True
            else:
                logger.error(
                    f"The provided taxonomy {sci_name} for sample {sample['alias']} is not submittable to ENA. Check https://ena-docs.readthedocs.io/en/latest/faq/taxonomy.html.")
                exit(1)
    except Exception as e:
        logger.error(
            f"An error occurred during the retrieval of {is_submittable_value} value from json data: {e}")
        exit(1)


def get_tax_by_taxid(json_data: list, taxid: int) -> list:
    try:
        for item in json_data:
            if isinstance(item, dict) and item.get("taxId") == str(taxid):
                if item.get("formalName") == "false":
                    return [item.get("scientificName"), item.get("scientificName")]
                else:
                    return [item.get("scientificName"), item.get("formalName")]
            else:
                logger.error(
                    f"An error occurred during the retrieval of scientific and common name for taxID {taxid}.")
                exit(1)
    except Exception as e:
        logger.error(
            f"An error occurred during the retrieval of taxonomic data from json data from taxId {taxid}: {e}")
        exit(1)

def get_unit(name: str, value):
    # Check if the unit is specified within value
    unit = "default"
    try:
        parts = str(value).split()
        if len(parts) > 1:
            potential_unit = parts[-1]
            if potential_unit in possible_unit[name]:
                unit = str(potential_unit)
                logger.trace(f"Unit '{unit}' found within value '{value}' for '{name}'.")
                return unit
            else:
                logger.trace(f"Potential unit '{potential_unit}' not recognized for '{name}'.")

         # if no unit is specified in within value, then use default unit 
        if unit == "default":
            unit = str(default_unit[name])
            logger.trace(f"Using default unit '{unit}' for '{name}' with numeric value '{value}'.")
            return unit

    except KeyError as e:
        logger.error(f"KeyError: '{e}' - Check if '{name}' exists in 'possible_unit' or 'default_unit'.")
    except Exception as e:
        logger.error(f"Unexpected error: {e}")


def add_sample_attribute_with_unit(doc, tag, text, name: str, value, unit: str) -> None:
    with tag("SAMPLE_ATTRIBUTE"):
        with tag("TAG"):
            text(name)
        with tag("VALUE"):
            text(strip_accents(control_missing_values(value)))
        with tag("UNITS"):
            text(unit)


def add_sample_attribute(doc, tag, text, name: str, value) -> None:
    with tag("SAMPLE_ATTRIBUTE"):
        with tag("TAG"):
            text(name)
        with tag("VALUE"):
            text(strip_accents(control_missing_values(value)))


def count_zeros_exceeds_threshold(lst: list, k: int) -> bool:
    try:
        # Check if the first argument is a list
        if not isinstance(lst, list):
            raise ValueError("The first argument must be a list.")

        # Check if the threshold k is an integer
        if not isinstance(k, int):
            raise ValueError("The threshold k must be an integer.")

        # Initialize the zero count
        zero_count = 0

        # Iterate through the list and count the zeros
        for item in lst:
            if str(item) == "0":
                zero_count += 1

        # Log the number of zeros found in the list
        #logger.info(
        #    f"The number of 0 values for the attributes in the sheet is {zero_count}.")

        # Return True if the number of zeros exceeds the threshold k, otherwise False
        return zero_count > k
    except Exception as e:
        # Log the error if any exception occurs
        logger.error(f"An error occurred: {e}")
        exit(1)

def isUselessValue(key, value):
    """
    Checks if the provided value matches the corresponding value in the useless_values dictionary for the given key.

    Parameters:
    key (str): The key to check in the useless_values dictionary.
    value (str): The value to compare against the value in the useless_values dictionary.

    Returns:
    bool: True if the key exists in useless_values and the value matches, False otherwise.
          If the key does not exist, returns False and logs a message.
    """
    try:
        # Check if the key exists in the dictionary
        if key in useless_values:
            # Compare the value with the dictionary's value for the given key
            return value in useless_values[key]
        else:
            return False
    except Exception as e:
        # Log any unexpected errors
        logger.error(f"An error occurred: {e}")
        exit(1)


def calculate_pass(key, sample, checklist, mandatory_attributes, joker, validator, zero_threshold, non_writable_attributes):
    """
    Calculate the value of `goto` based on various conditions.

    Parameters:
        key (str): The current key being evaluated.
        sample (dict): A dictionary containing sample data.
        checklist (list): A checklist for specificity validation.
        mandatory_attributes (list): A list of mandatory attributes.
        joker (list): A list of values considered as jokers.
        validator (int): Validator flag (0 or 1).
        zero_threshold (int): Threshold for zeros in sample values.
        non_writable_attributes (list): List of non-writable attributes.

    Returns:
        int: The value of `goto` (0 or 1).
    """
    goto = 1  # Default value of goto

    try:
        # Condition 1
        if handle_checklist_specificity([key], checklist)[0] not in mandatory_attributes and sample[key] in joker:
            goto = 0

        # Condition 2
        if (
            validator == 0 
            and count_zeros_exceeds_threshold(list(sample.values()), zero_threshold) 
            and sample[key] not in joker
        ):
            goto = 0

        # Condition 3
        if check_data_format(key, sample[key]) is False and sample[key] not in joker:
            goto = 0

        # Condition 4
        if isUselessValue(key, sample[key]) and key not in mandatory_attributes:
            goto = 0

        # Condition 5
        if key in non_writable_attributes:
            goto = 0

    except Exception as e:
        logger.error(f"Error calculating goto for key '{key}': {e}")
        goto = 0  # Default to 0 in case of error

    return goto



# Function to get info from samples sheet
def make_sample_xml(samples: dict, mandatory_attributes: list, ignore_submission_status: str, checklist: str, template_type: str) -> str:
    doc, tag, text = Doc().tagtext()
    # Initialize variables
    nb_sample = 0
    taxid_lst = []
    comnames_lst = []
    scinames_lst = []

    with tag("SAMPLE_SET"):
        for sample_alias in samples:
            nb_sample += 1
            sample = samples[sample_alias]
            submission_status = get_submission_status(
                sample, template_dict[template_type]["submission_status_key"], template_dict[template_type]["submission_status_yes"], ignore_submission_status)
            sub_key = template_dict[template_type]["submission_status_key"]
            for transformed_key in (sub_key.casefold(), sub_key.upper()):
                if transformed_key in sample:
                    sample.pop(transformed_key)
                    break            

            # tester la key "alias"
            if is_data_row(sample["alias"]) and submission_status == template_dict[template_type]["submission_status_yes"]:
                sample.pop("alias", None)
                with tag("SAMPLE", alias=sample_alias):
                    with tag("TITLE"):
                        text(strip_accents(sample["title"]))
                        sample.pop("title")

                    with tag("SAMPLE_NAME"):
                        tax_info = add_taxonomic_metadata(
                            doc, tag, text, sample, sample_taxonomic_attributes)
                        tax_json = get_json_data_from_curl(
                            ena_taxonomy_api+"/"+tax_info[1], tax_info[1])
                        if is_submittable_tax(sample, tax_json, tax_info[1], is_submittable_value):
                            logger.trace(
                                f"Processing taxonomy for taxID {tax_info[0]}")
                            tax_names_from_taxid = get_tax_by_taxid(
                                tax_json, tax_info[0])
                            if tax_names_from_taxid[0] == tax_info[1]:
                                taxid_lst.append(str(tax_info[0]))
                                scinames_lst.append(tax_info[1])
                                comnames_lst.append(tax_info[2])
                            sample.pop("taxon_id")
                            sample.pop("scientific_name")
                            sample.pop("common_name")

                    with tag("SAMPLE_ATTRIBUTES"):
                        for key in sample:
                            pass_token = 1
                            sample[key] = control_missing_values(sample[key])
                            validator = check_data_format(key, sample[key])
                            pass_token = calculate_pass(key, sample, checklist, mandatory_attributes, joker, validator, zero_threshold, non_writable_attributes)
                            if submission_status == template_dict[template_type]["submission_status_yes"] and pass_token == 1:
                                if key in default_unit.keys() and sample[key] not in joker:
                                    unit = get_unit(key, sample[key])
                                    # Add error catch here
                                    add_sample_attribute_with_unit(
                                        doc, tag, text, handle_checklist_specificity([key],checklist)[0], sample[key], unit)
                                else:
                                    add_sample_attribute(
                                        doc, tag, text, handle_checklist_specificity([key],checklist)[0], sample[key])

    logger.success(f"{str(nb_sample)} samples were processed :)")
    logger.info(
        f"{str(len(set(taxid_lst)))} different taxon ID(s) were found.")
    if len(set(taxid_lst)) != len(set(scinames_lst)) or len(set(taxid_lst)) != len(set(comnames_lst)):
        logger.error(
            f"Error: inconsistent number of taxon_id {str(len(set(taxid_lst)))} and scientific/common names {str(len(set(scinames_lst)))}, {str(len(set(comnames_lst)))} in the samples. Check spreadsheet file (are numbers automatically incremented in taxon_id column ?)")
        exit(1)

    result = indent(doc.getvalue())
    return (result)

# Function to get info from experiments sheet
def add_studyref_line_to_experiment(doc, tag, text, alias, experiment, template_type, study_ref) -> None:
    if template_type == "native":    
        if is_accession_id(experiment["STUDY_REF"]):
            doc.stag("STUDY_REF", accession=experiment["STUDY_REF"])
        else:
            doc.stag("STUDY_REF", refname=experiment["STUDY_REF"])
    else:
        if is_accession_id(study_ref):
            doc.stag("STUDY_REF", accession=study_ref)
        else:
            doc.stag("STUDY_REF", refname=study_ref)


def add_experiment_title(doc, tag, text, exp_title_key, experiment) -> None:
    try:
        with tag(exp_title_key):
            text(experiment[exp_title_key])
    except Exception as e:
        # Log the error if any exception occurs
        logger.error(
            f"An error occurred during the retrieval of experiment title: {e}")
        exit(1)


# fonction pour controller les key errors
library_layout_values = ["PAIRED", "SINGLE"]
def add_library_layout(doc, tag, text, experiment: dict, layout) -> None:
    try:
        if layout in library_layout_values:
            with tag("LIBRARY_LAYOUT"):
                if layout == "PAIRED":
                    if experiment["NOMINAL_LENGTH"] and is_valid_data_value(experiment["NOMINAL_LENGTH"]):
                        controlled_value = control_missing_values(
                            experiment["NOMINAL_LENGTH"])
                        doc.stag("PAIRED", NOMINAL_LENGTH=controlled_value)
                    else:
                        doc.stag("PAIRED")
                else:
                    doc.stag("SINGLE")
        else:
            logger.error(f"Provided library layout {layout} is not valid.")
            exit(1)
    except Exception as e:
        # Log the error if any exception occurs
        logger.error(
            f"An error occurred during the retrieval of library layout: {e}")
        exit(1)


def add_experiment_attribute(doc, tag, text, name: str, value) -> None:
    with tag("EXPERIMENT_ATTRIBUTE"):
        with tag("TAG"):
            text(name)
        with tag("VALUE"):
            text(strip_accents(control_missing_values(value)))



def make_experiment_xml(experiments: dict, ignore_submission_status: str, template_type: str, study_ref: str) -> str:
    doc, tag, text = Doc().tagtext()
    with tag("EXPERIMENT_SET"):

        for exp_alias in experiments:
            experiment = experiments[exp_alias]
            submission_status = get_submission_status(
                experiment, template_dict[template_type]["submission_status_key"], template_dict[template_type]["submission_status_yes"], ignore_submission_status)

            if is_data_row(experiment[template_dict[template_type]["exp_alias_key"]]) and submission_status == template_dict[template_type]["submission_status_yes"]:
                experiment.pop(template_dict[template_type]["exp_alias_key"])

                with tag("EXPERIMENT", alias=exp_alias):
                    add_experiment_title(
                        doc, tag, text, exp_title_key, experiment)
                    add_studyref_line_to_experiment(
                        doc, tag, text, exp_alias, experiment, template_type, study_ref)

                    with tag("DESIGN"):
                        doc.stag("DESIGN_DESCRIPTION")
                        doc.stag("SAMPLE_DESCRIPTOR", refname=experiment[template_dict[template_type]["SAMPLE_REF"]])
                        experiment.pop(template_dict[template_type]["SAMPLE_REF"])
                        #sample_descriptors = experiment["SAMPLE_DESCRIPTOR"].replace(';', ',').split(',')
                        #for sample_id in sample_descriptors:
                        #    sample_id = sample_id.strip()
                        #    if sample_id: 
                        #        doc.stag("SAMPLE_DESCRIPTOR", refname=sample_id)

                        with tag("LIBRARY_DESCRIPTOR"):

                            with tag("LIBRARY_NAME"):
                                text(experiment["LIBRARY_NAME"])
                            with tag("LIBRARY_STRATEGY"):
                                text(experiment["LIBRARY_STRATEGY"])
                            with tag("LIBRARY_SOURCE"):
                                text(experiment["LIBRARY_SOURCE"])
                            with tag("LIBRARY_SELECTION"):
                                text(experiment["LIBRARY_SELECTION"])

                            if is_valid_data_value(experiment["LIBRARY_LAYOUT"]):
                                add_library_layout(
                                    doc, tag, text, experiment, experiment["LIBRARY_LAYOUT"])

                    with tag("PLATFORM"):
                        with tag(experiment["PLATFORM"]):
                            with tag("INSTRUMENT_MODEL"):
                                text(experiment[template_dict[template_type]["INSTRUMENT"]])

                    with tag("EXPERIMENT_ATTRIBUTES"):
                        for key in experiment:
                            experiment[key] = control_missing_values(
                                experiment[key])
                            validator = check_data_format(key, experiment[key])
                            if validator and key.upper() not in experiment_mandatory_values and key.upper() not in experiment_special_keys:
                                add_experiment_attribute(
                                    doc, tag, text, key, experiment[key])

    result = indent(doc.getvalue())
    return (result)


def split_string_multiple_separators(string: str, separators: str) -> list:
    """
    Splits a string based on multiple separators.

    :param string: The input string to be split.
    :param separators: A list of separator characters.
    :return: A list of substrings.
    """
    # Create a regex pattern that matches any of the separators
    pattern = '|'.join(map(re.escape, separators))
    return re.split(pattern, string)


def find_md5_key(md5_file: str, filename: str) -> str:
    """
    This function takes a file containing MD5 sums and a filename as input,
    and returns the MD5 key associated with the given filename.

    :param md5_file: The path to the file containing MD5 sums.
    :param filename: The name of the file for which the MD5 key is to be found.
    :return: The MD5 key for the specified file, or None if the file is not found.
    """
    try:
        with open(md5_file, 'r') as f:
            logger.trace(f"Open MD5 file: {md5_file}")
            for line in f:
                # Assuming each line in the MD5 file is in the form: "md5sum  filename"
                parts = line.strip().split()
                if len(parts) == 2:
                    md5sum, file = parts
                    if file == filename:
                        logger.trace(f"Found MD5 sum for {filename}: {md5sum}")
                        return md5sum
                else:
                    logger.error(f"Wrong format for {filename}")
                    exit(1)
        logger.error(f"No MD5 sum found for {filename}")
        exit(1)
    except FileNotFoundError:
        logger.error(f"The file {md5_file} was not found.")
        exit(1)


# check data path
# Function to get info from runs sheet
def make_run_xml(runs: dict, upfile: str, md5sum_file: str, path_prefix: str, ignore_submission_status: str, template_type: str) -> str:
    doc, tag, text = Doc().tagtext()
    with tag("RUN_SET"):

        for run_alias in runs:
            run = runs[run_alias]

            submission_status = get_submission_status(
                run, template_dict[template_type]["submission_status_key"], template_dict[template_type]["submission_status_yes"], ignore_submission_status)

            if run["alias"] not in ["run_", "", str(0), "run_0"] and submission_status == template_dict[template_type]["submission_status_yes"]:
                if template_type != "native":
                    run_alias = "run_"+str(run_alias)
                with tag("RUN", alias=run_alias):
                    doc.stag("EXPERIMENT_REF", refname=run[template_dict[template_type]["EXPERIMENT_REF"]])

                    with tag("DATA_BLOCK"):

                        with tag("FILES"):
                            file_R1 = split_string_multiple_separators(
                                str(run[template_dict[template_type]["filename_r1"]]).strip().replace(" ", ""), separators)
                            for f1 in file_R1:
                                doc.stag("FILE", filename=f1, filetype=str(
                                    run["FILETYPE"]).lower(), checksum_method="MD5", checksum=find_md5_key(md5sum_file, str(f1)))
                                with open(upfile, "a") as file:
                                    file.write(
                                        path_prefix+"/"+str(run[template_dict[template_type]["data_dir"]])+"/"+str(f1)+"\n")

                            if str(run[template_dict[template_type]["filename_r2"]]) not in [0, "0", "", "None"] :
                                file_R2 = split_string_multiple_separators(
                                    str(run[template_dict[template_type]["filename_r2"]]).strip().replace(" ", ""), separators)
                                for f2 in file_R2:
                                    doc.stag("FILE", filename=f2, filetype=str(
                                        run["FILETYPE"]).lower(), checksum_method="MD5", checksum=find_md5_key(md5sum_file, str(f2)))
                                    with open(upfile, "a") as file:
                                        file.write(
                                            path_prefix+"/"+str(run[template_dict[template_type]["data_dir"]])+"/"+str(f2)+"\n")
    result = indent(doc.getvalue())
    return (result)


def import_metadata(file: str) -> dict:
    try:
        logger.info(f"Import metadata from {file} file...")
        metadata = get_data(file)
        logger.success(
            f"The data from {file} file have been successfully imported :) ")
        return metadata
    except Exception as e:
        logger.error(
            f"An error occurred during metadata import from {file}: {e}")
        exit(1)

def get_template_type_from_metadata(metadata: dict) -> str:
    """
    Checks the sheets present in the metadata dictionary and determines the template type
    according to the given criteria.

    :param metadata: The dictionary containing metadata with sheet names as keys
    :returns: The template type (template_type) based on the sheets present
    :rtype: str
    """
    try:
        # Get the sheet names from the metadata keys
        sheet_names = metadata.keys()

        # Check for "native" template type (all required sheets must be present)
        if all(sheet in sheet_names for sheet in native_template_sheet_names):
            logger.info("Template type: native")
            return "native"
        
        # Check for "egide_indiv" template type (all required sheets must be present but 'SAMPLES_meta' must be missing)
        elif all(sheet in sheet_names for sheet in egide_indiv_template_sheet_names) and 'SAMPLES_meta' not in sheet_names:
            logger.info("Template type: egide_indiv")
            return "egide_indiv"
        
        # Check for "egide_meta" template type (all required sheets must be present, including 'SAMPLES_meta')
        elif all(sheet in sheet_names for sheet in egide_meta_template_sheet_names):
            logger.info("Template type: egide_meta")
            return "egide_meta"
        
        # If none of the conditions match, return "unknown"
        logger.warning("Template type: unknown")
        return "unknown"
    
    except Exception as e:
        # Log error in case of exception
        logger.error(f"An error occurred while processing metadata: {e}")
        exit(1)


def make_authors_list(data: dict) -> list:
    for sheet in data:
        if sheet == collaborators_sheet_name:
            logger.info(
                f"{sheet} data sheet found, getting list of authors for this study.")
            authors = excel_sheet_to_dict(data, [sheet])
            author_list = get_authors(authors)
            logger.success(
                f"Collaborators list successfully retrieved: {author_list} :)")
    return author_list


def check_file_format(data: dict, template_type: str, nb_row_to_skip=0) -> None:
    """
    This function takes data from a xlsx file and return a boolean : true if file not empty and with correct sheets. false if not

    :param data: data from xlsx file
    :return: none
    """
    logger.info(f"Testing if the file form is correct...")

    if template_type == "native":
        sheet_list = native_sheet_list
        project_sheet = "project"
    else:
        sheet_list = egide_sheet_list
        project_sheet = "STUDY"

    sheets_in_file = [sheet for sheet in data]

    # Test if the correct sheets are in the file
    list_of_missing_sheets = []
    for sheet in sheet_list:
        if sheet not in sheets_in_file:
            list_of_missing_sheets.append(sheet)

    if list_of_missing_sheets == []:
        logger.success(f"All required sheets for a {template_type} template are present  :)")
    else:
        logger.error(f"Some sheets are mising for a {template_type} template : {list_of_missing_sheets}")
        exit(1)

    # Test if the alias in study sheet is at least completed
    project_dict = excel_sheet_to_dict(data, sheet_names_dict[template_type]["project"], template_dict[template_type]["nb_row_to_skip"])
    if project_dict != {}:
        logger.success(f"Mandatory alias is at least completed :)")
    else:
        logger.error(f"Mandatory alias is not completed")
        exit(1)
    logger.success(f"The file has a correct format  :)")


def from_tab_to_xml(data: dict, output_dir: str, upfile: str, max_project: int, attributes: list, author_list: list, ignore_submission_status: str, isBrokering: str, md5sum_file: str, path_prefix: str, is_project_converted: str, is_run_converted: str, is_sample_converted: str, is_experiment_converted: str, template_type:str) -> None:
    if is_project_converted == "True" :
        logger.info("Generating project XML...")
        project_sheet_to_xml(data, output_dir, max_project, author_list, isBrokering, template_type)
    if is_run_converted == "True" :
        logger.info("Generating run XML...")
        run_sheet_to_xml(data, output_dir, upfile, ignore_submission_status, md5sum_file, path_prefix, template_type)
    if is_sample_converted == "True" :
        logger.info("Generating sample XML...")
        sample_sheet_to_xml(data, output_dir, attributes, author_list, ignore_submission_status, template_type)
    if is_experiment_converted == "True" :
        logger.info("Generating experiment XML...")
        experiment_sheet_to_xml(data, output_dir, ignore_submission_status, template_type)


def run_sheet_to_xml(data: dict, output_dir: str, upfile: str, ignore_submission_status: str, md5sum_file: str, path_prefix: str, template_type: str) -> None:
    output_dir = os.path.normpath(output_dir)
    logger.info(f"Processing run data sheet...")
    runs = excel_sheet_to_dict(data, sheet_names_dict[template_type]["run"],template_dict[template_type]["nb_row_to_skip"])
    with open(output_dir+"/run.xml", "w") as file:
        file.write(header_xml + "\n")
        file.write(make_run_xml(runs, upfile, md5sum_file,
                   path_prefix, ignore_submission_status, template_type))


def experiment_sheet_to_xml(data: dict, output_dir: str, ignore_submission_status: str, template_type: str) -> None:
    output_dir = os.path.normpath(output_dir)
    logger.info(f"Processing experiment data sheet...")
    projects = excel_sheet_to_dict(data, sheet_names_dict[template_type]["project"],template_dict[template_type]["nb_row_to_skip"])
    study_ref = get_study_ref(projects, template_dict[template_type]["project_alias_key"])
    logger.success(f"Study alias successfully retrieved: {study_ref}")
    experiments = excel_sheet_to_dict(data, sheet_names_dict[template_type]["experiment"], template_dict[template_type]["nb_row_to_skip"])
    with open(output_dir+"/experiment.xml", "w") as file:
        file.write(header_xml + "\n")
        file.write(make_experiment_xml(experiments, ignore_submission_status, template_type, study_ref))


def sample_sheet_to_xml(data: dict, output_dir: str, attributes: list, author_list: list, ignore_submission_status: str, template_type: str) -> None:
    output_dir = os.path.normpath(output_dir)
    logger.info(f"Processing sample data sheet...")
    samples = excel_sheet_to_dict(data, sheet_names_dict[template_type]["sample"],template_dict[template_type]["nb_row_to_skip"])
    logger.info(f"Extracting checklist from sample sheet(s)...")
    checklist = get_checklist(samples, template_type)
    logger.success(f"Checklist successfully retrieved: {checklist} :)")
    logger.info(f"Checking attributes compliance with checklist {checklist}...")
    projects = excel_sheet_to_dict(data, sheet_names_dict[template_type]["project"],template_dict[template_type]["nb_row_to_skip"])
    study_type = get_study_type(projects,  template_dict[template_type]["study_type_key"])
    attributes = handle_checklist_specificity(attributes, checklist)
    is_compliant(checklist, attributes, study_type, template_type)
    mandatory_attributes = get_mandatory_attributes(checklist, study_type, template_type)
    mandatory_attributes = handle_checklist_specificity(mandatory_attributes, checklist)
    logger.success(f"Sample attributes are compliant with {checklist} :)")
    with open(output_dir+"/sample.xml", "w") as file:
        file.write(header_xml+"\n")
        file.write(make_sample_xml(
            samples, mandatory_attributes, ignore_submission_status, checklist, template_type))


def project_sheet_to_xml(data: dict, output_dir: str, max_project: int, author_list: list, isBrokering: str, template_type: str) -> None:
    output_dir = os.path.normpath(output_dir)

    logger.info(f"Processing project data sheet...")
    projects = excel_sheet_to_dict(data, sheet_names_dict[template_type]["project"],template_dict[template_type]["nb_row_to_skip"])

    logger.info(f"Extracting center name from projet sheet...")
    center_name = get_center_name(projects, template_dict[template_type]["center_name_key"])
    logger.success(f"Center name successfully retrieved: {center_name} :)")

    logger.info(f"Extracting study type from project sheet...")
    study_type = get_study_type(projects, template_dict[template_type]["study_type_key"])
    logger.success(f"Study type successfully retrieved: {study_type} :)")

    logger.info(f"Submission for {center_name}")
    with open(output_dir+"/project.xml", "w") as file:
        file.write(header_xml+"\n")
        file.write(make_project_xml(
            projects, max_project, author_list, isBrokering, template_type))



def print_file_list(data: dict, prefix: str, output_file: str, separators: list, template_type: str):
    """
    Writes a list of file paths to an output file based on the metadata in `data`.

    :param data: Dictionary containing metadata where each item has a 'data_dir', 'filename_r1', and 'filename_r2'
    :param prefix: The prefix to prepend to each file path
    :param output_file: The file where the file paths will be written
    :param separators: List of separators to use when splitting filenames
    """
    try:
        # Open the output file for writing
        with open(output_file, "w") as file:
            logger.info(f"Writing file paths to {output_file}...")

            # Iterate through each item in the metadata dictionary
            for key, subdict in data.items():

                # Get the path from either 'data_dir' or 'DATA_DIRECTORY'
                path = subdict.get(template_dict[template_type]["data_dir"])
                if not path:
                    logger.error(f"Missing 'data_dir' or 'DATA_DIRECTORY' in entry {key}, skipping.")
                    exit(1)
                
                R1 = subdict.get(template_dict[template_type]["filename_r1"])
                R2 = subdict.get(template_dict[template_type]["filename_r2"])
                # Process R1 (if valid)
                if R1 and R1 != str(0):
                    elements_R1 = split_string_multiple_separators(R1, separators)
                    for element in elements_R1:
                        concat_r1 = f"{prefix}/{path}/{element}"
                        file.write(concat_r1.replace("//","/") + '\n')
                    logger.trace(f"Processed R1 files for key: {key}")

                # Process R2 (if valid)
                if R2 and R2 != str(0) and R2 != "None":
                    elements_R2 = split_string_multiple_separators(R2, separators)
                    for element in elements_R2:
                        concat_r2 = f"{prefix}/{path}/{element}"
                        file.write(concat_r2.replace("//","/") + '\n')
                    logger.trace(f"Processed R2 files for key: {key}")

        logger.success(f"File paths successfully written to {output_file}")

    except Exception as e:
        logger.error(f"An error occurred while processing the file list: {e}")
        exit(1)

##########################################################################################
#                			MAIN  						 #
##########################################################################################
def main(opts):
    # check if input file exist and create output directory if not existing
    #make_dir_if_not_exist(opts.output_dir)
    check_file_exists(opts.input_file, "input file")

    # import metadata spreadsheet file
    submit_data = import_metadata(opts.input_file)

    # get template type
    template_type = get_template_type_from_metadata(submit_data)

    # get list of attributes
    attributes = get_attribute_list(submit_data, template_type)

    #Check file format
    check_file_format(submit_data, template_type, template_dict[template_type]["nb_row_to_skip"])

    # Configure handler for TRACE level only
    logger.add(opts.trace, level="TRACE", format="{time} {level} {message}", filter=lambda record: record["level"].name == "TRACE")

    if opts.only_names == "True" :
        run = excel_sheet_to_dict(submit_data, sheet_names_dict[template_type]["run"], template_dict[template_type]["nb_row_to_skip"])
        print_file_list(run, opts.path_prefix, opts.upupfile, separators, template_type)

    else :
        #check if md5 file exists
        check_file_exists(opts.md5_file, "md5file")

        #get list of authors
        author_list = make_authors_list(submit_data)

        # Convert Excel/ODS file to XML
        from_tab_to_xml(submit_data, opts.output_dir, opts.upfile, opts.maxp, attributes, author_list, opts.ignoreSubStatus, opts.isBrokering, opts.md5_file, opts.path_prefix, opts.is_project_converted, opts.is_run_converted, opts.is_sample_converted, opts.is_experiment_converted, template_type)
  
        basename = opts.input_file.rsplit("/", 1)[-1] 
        logger.success(f"XML files successfully generated from {basename} file.")


if __name__ == "__main__":
    opts = set_up_argparse()
    main(opts)
