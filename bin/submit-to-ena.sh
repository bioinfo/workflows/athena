#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                       Submit sequence records to ENA                      ##
##                                                                           ##
###############################################################################
# var settings
args=("$@")
PROJECT=${args[0]}
SAMPLE=${args[1]}
EXPERIMENT=${args[2]}
RUN=${args[3]}
ACTION=${args[4]}
URL=${args[5]}
RELEASE_DATE=${args[6]}
CENTRE_NAME=${args[7]}
ENA_CRED=${args[8]}
RECEIPT=${args[9]}
GET_STUDY=${args[10]}
GET_SAMPLE=${args[11]}
GET_EXPERIMENT=${args[12]}
GET_RUN=${args[13]}

# Read user and password from credentials file
read USR PWD < $ENA_CRED

# Build cURL command
curl_command='curl --user $USR:$PWD --form "ACTION=${ACTION}" --form "HOLD=${RELEASE_DATE}" --form "CENTER_NAME=${CENTRE_NAME}"'

# Add XML forms conditionnally
if [ "${GET_STUDY}" = "True" ]; then
    curl_command+=' --form "PROJECT=@${PROJECT}"'
fi

if [ "${GET_SAMPLE}" = "True" ]; then
    curl_command+=' --form "SAMPLE=@${SAMPLE}"'
fi

if [ "${GET_EXPERIMENT}" = "True" ]; then
    curl_command+=' --form "EXPERIMENT=@${EXPERIMENT}"'
fi

if [ "${GET_RUN}" = "True" ]; then
    curl_command+=' --form "RUN=@${RUN}"'
fi

# Complete URL commande
curl_command+=' --url ${URL} > ${RECEIPT}'

# Print and execute cURL command
echo ${curl_command}
eval $curl_command

#######################################################
########              HELP SECTION             ########
#######################################################
# DESCRIPTION
# curl  is  a  tool  to  transfer data from or to a server, using one of the supported protocols
# (DICT, FILE, FTP, FTPS, GOPHER, HTTP, HTTPS, IMAP, IMAPS, LDAP, LDAPS, POP3, POP3S, RTMP, RTSP, SCP, SFTP, SMTP, SMTPS, TELNET and TFTP).
# The command is designed to work without user interaction.
# curl offers a busload of useful tricks like proxy support, user authentication, FTP upload, HTTP post, SSL connections, cookies, file transfer resume, Metalink, and more.
# As  you will see below, the number of features will make your head spin!
# curl is powered by libcurl for all transfer-related features. See libcurl(3) for details.
#[...]
#       -F, --form <name=content>
#              (HTTP) This lets curl emulate a filled-in form in which a user has pressed the submit button. This causes curl to POST data using the Content-Type  multi-
#              part/form-data according to RFC 2388. This enables uploading of binary files etc. To force the 'content' part to be a file, prefix the file name with an @
#              sign. To just get the content part from a file, prefix the file name with the symbol <. The difference between @ and < is then that @  makes  a  file  get
#              attached in the post as a file upload, while the < makes a text field and just get the contents for that text field from a file.
#              Example, to send your password file to the server, where 'password' is the name of the form-field to which /etc/passwd will be the input:
#              curl -F password=@/etc/passwd www.mypasswords.com
#              To read content from stdin instead of a file, use - as the filename. This goes for both @ and < constructs.
#              You can also tell curl what Content-Type to use by using 'type=', in a manner similar to:
#              curl -F "web=@index.html;type=text/html" url.com
#              or
#              curl -F "name=daniel;type=text/foo" url.com
#              You can also explicitly change the name field of a file upload part by setting filename=, like this:
#              curl -F "file=@localfile;filename=nameinpost" url.com
#              If filename/path contains ',' or ';', it must be quoted by double-quotes like:
#              curl -F "file=@\"localfile\";filename=\"nameinpost\"" url.com
#              or
#              curl -F 'file=@"localfile";filename="nameinpost"' url.com
#              Note that if a filename/path is quoted by double-quotes, any double-quote or backslash within the filename must be escaped by backslash.
#              See further examples and details in the MANUAL.
#              This option can be used multiple times.
#
#       -T, --upload-file <file>
#              This  transfers  the  specified local file to the remote URL. 
#	       If there is no file part in the specified URL, Curl will append the local file name. NOTE that you must use a
#              trailing / on the last directory to really prove to Curl that there is no file name or curl will think that your last directory name is the remote file name to  use.  That
#              will most likely cause the upload operation to fail. If this is used on an HTTP(S) server, the PUT command will be used.
#
#              Use  the file name "-" (a single dash) to use stdin instead of a given file.  Alternately, the file name "." (a single period) may be specified instead of "-" to use stdin
#              in non-blocking mode to allow reading server output while stdin is being uploaded.
#
#              You can specify one -T for each URL on the command line. Each -T + URL pair specifies what to upload and to where. curl also supports "globbing" of the -T argument,  mean-
#              ing that you can upload multiple files to a single URL by using the same URL globbing style supported in the URL, like this:
#
#              curl -T "{file1,file2}" http://www.uploadtothissite.com
#
#              or even
#
#              curl -T "img[1-1000].png" ftp://ftp.picturemania.com/upload/
#
#       -u, --user <user:password>
#              Specify the user name and password to use for server authentication. Overrides -n, --netrc and --netrc-optional.
#
#              If you simply specify the user name, curl will prompt for a password.
#
#              If you use an SSPI-enabled curl binary and perform NTLM authentication, you can force curl to select the user name and password from your environment by specifying a  sin-
#              gle colon with this option: "-u :".
#
#              If this option is used several times, the last one will be used.
#
#       -U, --proxy-user <user:password>
#              Specify the user name and password to use for proxy authentication.
#
#             If  you  use  an SSPI-enabled curl binary and do NTLM authentication, you can force curl to pick up the user name and password from your environment by simply specifying a
#              single colon with this option: "-U :".
#
#              If this option is used several times, the last one will be used.
#
#       --url <URL>
#              Specify a URL to fetch. This option is mostly handy when you want to specify URL(s) in a config file.
#
#              This option may be used any number of times. To control where this URL is written, use the -o, --output or the -O, --remote-name options.
#
#[...]
#######################################################
