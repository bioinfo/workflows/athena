# Change Log
All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
 
## [2.0] - 2022-09-13
  
Upgrade to version 2.0. includes complete migration to [Nextflow](https://www.nextflow.io/) for athENA pipeline.

### Added
- [Nextflow wrapper](https://gitlab.ifremer.fr/bioinfo/athena/-/issues/2)
- [Conda environment export to Datarmor](https://gitlab.ifremer.fr/bioinfo/athena/-/issues/1)

 
### Changed
- [Export validate and upload steps](https://gitlab.ifremer.fr/bioinfo/athena/-/issues/3)

 
### Fixed

 
